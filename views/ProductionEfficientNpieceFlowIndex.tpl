<!DOCTYPE html>
<meta charset="utf-8" />
<title>生产效率实验多件流</title>
    <link rel="icon" 
          type="image/png" 
          href="/images/logo_pure.png">
    <link href="/bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">    
    <link rel="stylesheet" href="/bootstrap/css/font-awesome.min.css">
    <link rel='stylesheet' href='/stylesheets/c3.css' />
    <link href="/stylesheets/style.css" rel="stylesheet" media="screen">    
    <script type="text/javascript" src = "/javascripts/jquery.js"></script>
    <script type="text/javascript" src = "/javascripts/d3.js"></script>
    <script type="text/javascript" src = "/javascripts/c3.js"></script>
    <script type="text/javascript" src = "/javascripts/underscore.js"></script>
 
<script language="javascript" type="text/javascript">
    // var wsUri = "ws://localhost:3003/";
	var wsUri = 'ws:' + window.location.href.substring(window.location.protocol.length)+ 'ws?guid={{.guid}}';
    var chart;
    var output;
    var columns = []
    var nPieceFlowMessages = []

    function setToLineChart(){
        // if(chartStyle == 'line') return
        // chartStyle = 'line'
        chart = c3.generate({
            data: {
                    xs: {
                        '三件流': 'x1',//x1 代表标识在x轴上的数据数组，与之对应的y轴上的数据是 三件流 的数据数组
                        '单件流': 'x2',
                    },
                    columns: buildLineChartData(),
                    type: 'spline',
                    // [
                    //     ['x1', 0],
                    //     ['x2', 0],
                    //     ['三件流', 0],
                    //     ['单件流', 0]
                    // ]
            },
            grid: {
                    x: {
                        show: true
                    },
                    y: {
                        show: true
                    }
            },             
            point: { r: 5 },
            axis: {
                x: {
                    label: {
                        text: '时间',
                        position: 'outer-right',
                    }
                },
                y: {
                    label: {
                        text: '订单完成率',
                        position: 'outer-middle',
                    }
                },
            },                         
        });  
    }    
    function buildLineChartData(){
        if(_.size(nPieceFlowMessages) <= 0){
            var rtn = [['x1', 0, 15, 30], ['x2', 0, 10, 20], ['三件流', 0, 30, 50], ['单件流', 0, 50, 70]]
            console.log(JSON.stringify(rtn))
            return rtn
        }
        var x1 = ['x1']
        var three = ['三件流']
        
        var x2 = ['x2']
        var single = ['单件流']

        _.each(nPieceFlowMessages, function(_message){
            if(_message.GameStateFlag == 5){//多件流
                x1.push(_message.TimeElapse)
                three.push(_message.CoverRate)
            }else if(_message.GameStateFlag == 6){//单件流
                x2.push(_message.TimeElapse)
                single.push(_message.CoverRate)
            }
        }) 
        var rtn = [x1, x2, three, single]
        console.log(JSON.stringify(rtn))
        return  rtn
    }
    function updateNPieceFlowMessage(message){
        // nPieceFlowMessages.push(message)
        if(_.isArray(message)){
            for (var i = message.length - 1; i >= 0; i--) {
                var m = message[i]
                console.log("GameStateFlag: %d | name: %s | rate: %d | timestamp: %d", m.GameStateFlag, m.Name, m.CoverRate, m.TimeElapse)
                nPieceFlowMessages.push(m)
            };
        }      
        chart.load({
            columns: buildLineChartData(),
        });          
    }
    function init() {
        output = document.getElementById("output");

        $.get("../OrderCoverResults?state=6", function(data){
            if(data != null){
                nPieceFlowMessages = data
            }
            setToLineChart()
            createWebSocket()            
        })        

    }

    function onMessage(evt) {
        // writeToScreen('<span style="color: blue;">RESPONSE: ' + evt.data+'</span>');
        var cmd = JSON.parse(evt.data);
        // console.log(cmd)
        switch(cmd.Type){
            case 4:
                writeToScreen(cmd.Content)
                break;
            case 10:
                console.log(cmd.Content)
                var data = JSON.parse(cmd.Content)
                // console.log(data)
                // console.log(data.Result.timeElapse)
                updateNPieceFlowMessage(data)
                break
            // case 7:
            //     console.log('生产计划已经完成')
            //     setToPieChart()
            // break
        }
        // if(cmd.Type==4){
        //     writeToScreen(cmd.Content)
        // }
    }
    function createWebSocket() {
        websocket = new WebSocket(wsUri);
        websocket.onopen = function (evt) { onOpen(evt) };
        websocket.onclose = function (evt) { onClose(evt) };
        websocket.onmessage = function (evt) { onMessage(evt) };
        websocket.onerror = function (evt) { onError(evt) };
    }
    function onOpen(evt) {
        writeToScreen("已连接到服务器");
    }
    function onClose(evt) {
        writeToScreen("连接已断开");
        websocket=null

    }

    function onError(evt) {
        writeToScreen('<span style="color: red;">ERROR:</span> ' + evt.data);
        websocket=null
    }
    function doSend(message) {
        writeToScreen("SENT: " + message);
        websocket.send(message);
    }
    function writeToScreen(message) {
        return
        var pre = document.createElement("p");
        pre.style.wordWrap = "break-word";
        pre.innerHTML = message;
        output.appendChild(pre);
    }
    function checkConnectionStatus(){
      if(websocket == null){
        createWebSocket()
      }
    }    
    window.addEventListener("load", init, false);
</script>

<!-- <h2>N件流</h2> -->
<body> 

    <div id = "divNav">
        <div class="container">
            <div class="row"  id = "">
                <div class="col-xs-1 col-sm-1 col-md-1 col-md-lg-1">

                    <img id="imglogo" src="/images/logo3.png">
                </div>
                <div class="col-xs-11 col-sm-11 col-md-11 col-md-lg-11">
                    <div style="">
                           <a class= "navFunc" style="right:6px;" onclick = "openAboutWindow()">关于 </a>
                     </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row" id = "subNavBar" style="">
            <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12">
                <div id="lblTableTitleText" style=""> 
                  <span class="glyphicon glyphicon-arrow-right" style="margin-right: 3px;"></span>
                  <span style="font-size: 16px;"> 生产效率实验订单完成率实时统计图 </span></div>
            </div>
         </div>
     </div>  

    <div class="container">
        <div class="row chartOutterContainer"  id = "row_container">
            <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12" style="margin-top:5px;">
                <div id="chart" style="" class="chartInnerContainer"></div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12" style="margin-top: 0px;margin-bottom: 20px;text-align:center;">
                <div class="icon-spinner icon-spin icon-2x pull-left" style="width:100%"> </div>
                <div class="" style="text-align: center; color: rgba(150,150,150,0.8);margin-top: 40px;" > 数据接收中...</div>   
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12" id = "tipContainer" style="">
            <div> 
                <span class="icon-pushpin" style="margin-right: 3px;"></span>
                <span style=""> 上图展示了实验过程中在特定时间的订单实时完成率 </span>
            </div>
          </div>
        </div>
    </div>   
<!-- Modal -->
    <script language="javascript" type="text/javascript">
        function openAboutWindow(){
          $('#modalAbout').modal();
        }
    </script>
<div class="modal fade" id="modalAbout" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #00695c; color: white;">
        <button type="button" class="close" data-dismiss="modal"><!-- <span aria-hidden="true">&times;</span> --><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">关于</h4>
      </div>
      <div class="modal-body">
        <div style="text-align: center; font-size: 19px; margin-top: 20px;">
           {{.aboutInfo}}
        </div>
      </div>
      <div class="modal-footer" style="text-align: center;">
        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>
<!-- Modal End-->    
    <script src="/bootstrap/js/bootstrap.min.js"></script>
</body>



<div id="output"></div>
</html>
  
  