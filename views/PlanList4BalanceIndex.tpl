<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>生产平衡实验计划设置</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" 
          type="image/png" 
          href="/images/logo_pure.png">
    <!-- Loading Bootstrap -->
    <!-- <link href="bootstrap/css/bootstrap.css" rel="stylesheet"> -->
    <!-- <link href="/stylesheets/headroom.css" rel="stylesheet" media="screen">     -->
    <link href="/bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">    
    <link rel="stylesheet" href="/bootstrap/css/font-awesome.min.css">
    <link href="/stylesheets/style.css" rel="stylesheet" media="screen">    
    <link href="/dataTable/jquery.dataTables.css" rel="stylesheet" media="screen">    
    <style type="text/css">
      td.highlight {
          background-color: rgba(0,256,0, 0.1) !important;
      }
      tr.highlight{
          background-color: rgba(0,256,0, 0.1) !important;
      }
      #lblTip{
        margin-top: 30px; text-align: center; font-size: 30px; color: red; margin-bottom: 20px;
      }
      #dtProcess{
        text-align: center;
      }
      th{
        text-align: center;
      }
      .btn{
        width: 100px;
      }
 
    </style>
    <!-- // <script data-main="javascripts/main" src="/javascripts/require.js"></script> -->
    <script type="text/javascript" src = "/javascripts/jquery.js"></script>
    <script type="text/javascript" src = "/javascripts/underscore.js"></script>
    <!-- // <script type="text/javascript" src = "/javascripts/headroom.min.js"></script> -->
    <script type="text/javascript">
        var wsUri = 'ws:' + window.location.href.substring(window.location.protocol.length) + 'ws?guid={{.guid}}';
        // var wsUri = "ws://localhost:8080/planConfigIndexws";
        var cmds = [];
        var table;

        $(document).ready(function() {

            table = $('#dtProcess').DataTable( {
                "paging":   false,
                "ordering": false,
                "info":     false,
                "searching":  false,
                "ajax": "../PlanList4Production?value=2",
                "columns": [
                    { "data": "index" ,"width": "33%"},
                    { "data": "name" ,"width": "33%"},
                    { "data": "quantity" ,"width": "33%"},
                    // { "data": "" },
                ]
            } );

            $('#dtProcess tbody').on( 'click', 'tr', function () {
                if ( $(this).hasClass('selected') ) {
                    $(this).removeClass('selected');
                }
                else {
                    table.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                }
            } );
            
            table.on('draw', function(){
                return;
                var rowsCount = table.rows()[0].length;
                for(var i = 0; i< rowsCount; i++){
                    var data = table.row().nodes().data()[i];
                    console.dir(data);
                    if(data.complted == true){
                        $(table.row(i).node()).addClass('highlight');
                    }
                }
            })


            // $("#checkAutoPlan").change(checkboxChange);

            // createWebSocket();
            // updateSaying();
            // setInterval(updateSaying, 30 * 1000);

        });
        function start(){
          window.open("../BalanceProductionIndex")
        }
        function updateSaying(){
            $.get('../GetRandomSaying', function(_data){
                var saying = JSON.parse(_data);
                $('#sayingContent').text(saying.Content);
                $('#sayingAuthor').text(saying.Author);
            })
        }        

        function createWebSocket() {
            websocket = new WebSocket(wsUri);
            websocket.onopen = function (evt) { onOpen(evt) };
            websocket.onclose = function (evt) { onClose(evt) };
            websocket.onmessage = function (evt) { onMessage(evt) };
            websocket.onerror = function (evt) { onError(evt) };
        }
        function onMessage(evt) {
            var _cmd = JSON.parse(evt.data);
            console.log(_cmd);
            if(_cmd.cmd == 'reload'){
              table.ajax.reload();
              console.log(_cmd.message);
              showMessage(_cmd.message);
            }else{
              console.log(_cmd.message);
              showMessage(_cmd.message);
            }
        }   
        function onOpen(evt) {
          console.log('websocket open');
          websocket.send('');
        }
        function onClose(evt) {
          console.warn('websocket closed');
        }
        function onError(evt) {
          console.error('websocket error');
        }            
        function hideTip(){
            $('#lblTip').css('display', 'none');
            $('#lblTableTitleText').css('margin-top', '40px');
        }
        function showMessage(_msg){
          if(_msg != null && _msg.length > 0){
              var ele = 
                '<div class="alert alert-success alert-dismissible" role="alert" >'+
                    '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'+
                    '<div> '+ _msg +'</div>'+
                '</div>';
              $('#messageContainer').append(ele);            
          }

        }
    </script>
  </head>
  <body>
    <div id = "divNav">
        <div class="container">
            <div class="row"  id = "">
                <div class="col-xs-1 col-sm-1 col-md-1 col-md-lg-1">

                    <img id="imglogo" src="/images/logo3.png">
                </div>
                <div class="col-xs-11 col-sm-11 col-md-11 col-md-lg-11">
                    <div style="">

<!--                           <a class= "navFunc" style="right:525px;">教程 </a>
                          <a class= "navFunc" style="right:470px;"  href="/orderConfigIndex" target="_blank">订单 </a>
                          <a class= "navFunc " style="right:415px;" href="/processConfigIndex" target="_blank">流程 </a>
                          <a class= "navFunc " style="right:360px;color: rgb(100,100,100);" href="/planConfigIndex" target="_blank">计划 </a>
                          <a class= "navFunc" style="right:240px;"  href="/barchartsIndex" target="_blank">实时时长统计 </a>
                          <a class= "navFunc" style="right:120px;"  href="/piechartsIndex" target="_blank">实时比重统计 </a>
                          <a class= "navFunc" style="right:70px;">导出 </a>
 -->                          
                         <a class= "navFunc" style="right:6px;" onclick = "openAboutWindow()">关于 </a>
                          <!-- <a class= "navFunc" style="right:20px;" onclick = "openAboutWindow()">关于 </a> -->
                     </div>
                </div>
            </div>
        </div>
    </div>

<!--     <div class="demo-panel-title"> 
        <div class = "container">
            <div class="row" style="margin-top:20px;">
                <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12">
                    <div id= "sayingContent" style=""> </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12">
                    <div id="sayingAuthor" style=""></div>
                </div>
                
            </div>
        </div>
    </div> -->
    <div class="container">
        <div class="row" id = "subNavBar" style="">
            <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12">
                <div id="lblTableTitleText" style=""> 
                  <span class="glyphicon glyphicon-arrow-right" style="margin-right: 3px;"></span>
                  <span style="font-size: 16px;"> 生产平衡实验计划列表 </span></div>
            </div>
         </div>
     </div>    
    <div class="container" id= "mainContainer">
      <div class="row"  id = "row_container">
        <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12">
          <table id="dtProcess" class="display" cellspacing="0" width="100%" >
              <thead>
                  <tr>
                      <th>计划编码</th>
                      <th>产品名称</th>
                      <th>生产数量</th>
                      <!-- <th>备注</th> -->
                  </tr>
              </thead>
          </table>
        </div>
        <!-- <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12" style="margin-top: 50px;text-align:center;"> </div> -->
        <!-- <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12" style="margin-top: 15px;text-align:center;" id="messageContainer"> </div> -->
      </div>
      <div class="row" style="margin-top: 150px;margin-bottom: 10px;text-align:center;">
            <button class="btnAction" onclick= "start()" style="">开始</button>
      </div>
    </div>
    <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12" id = "tipContainer" style="">
            <div> 
                <span class="icon-pushpin" style="margin-right: 3px;"></span>
                <span style=""> 列表中列出了将要进行的生产计划 </span>
            </div>
          </div>
        </div>
    </div>  
<!-- Modal -->
    <script language="javascript" type="text/javascript">
        function openAboutWindow(){
          $('#modalAbout').modal();
        }
    </script>
<div class="modal fade" id="modalAbout" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #00695c; color: white;">
        <button type="button" class="close" data-dismiss="modal"><!-- <span aria-hidden="true">&times;</span> --><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">关于</h4>
      </div>
      <div class="modal-body">
        <div style="text-align: center; font-size: 19px; margin-top: 20px;">
           {{.aboutInfo}}
        </div>
      </div>
      <div class="modal-footer" style="text-align: center;">
        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>
<!-- Modal End-->    
    <script src="/bootstrap/js/bootstrap.min.js"></script>
    <script src="/layer/layer.min.js"></script>
    <script src="/dataTable/jquery.dataTables.js"></script>
   
  </body>
</html>
