<!DOCTYPE html>
<meta charset="utf-8" />
<title>开始实验</title>
    <link rel="icon" 
          type="image/png" 
          href="/images/logo_pure.png">
    <link href="/bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">    
    <link rel="stylesheet" href="/bootstrap/css/font-awesome.min.css">
    <link href="/stylesheets/style.css" rel="stylesheet" media="screen">    
    <style type="text/css">
      body{
        background-color:rgb(238,238,238);
      }
    </style>    
    <script type="text/javascript" src = "/javascripts/jquery.js"></script>
    <script type="text/javascript" src = "/javascripts/underscore.js"></script>

    <script language="javascript" type="text/javascript">
        var output;
        function startProductionBalance(){
          window.open("../PlanList4BalanceIndex")
            // window.location.href="../PlanList4BalanceIndex"
        }
        function startQuanlifiedProduction(){
          window.open("../PlanList4QuanlifiedProductionNPieceIndex")
            // window.location.href="../PlanList4QuanlifiedProductionNPieceIndex"
            // window.location.href="../PlanList4QuanlifiedProductionSinglePieceIndex"

        }
        function startProductionEfficient(){
          window.open("../OrderList4ProductionEfficientIndex")
            // window.location.href="../OrderList4ProductionEfficientIndex"
        }
        function init() {
            // output = document.getElementById("output");
                       
        }
        function writeToScreen(message) {
            var pre = document.createElement("p");
            pre.style.wordWrap = "break-word";
            pre.innerHTML = message;
            output.appendChild(pre);
        }

        window.addEventListener("load", init, false);
    </script>

<!-- <h2>N件流</h2> -->
<body> 
    <div id = "divNav" style=""> 
        <div class="container">
            <div class="row"  id = "" style="">
                <div class="col-xs-1 col-sm-1 col-md-1 col-md-lg-1">

                    <img id="imglogo" src="/images/logo3.png">
                </div>
                <div class="col-xs-11 col-sm-11 col-md-11 col-md-lg-11">
                    <div style="">
                          <a class= "navFunc" style="right:6px;" onclick = "openAboutWindow()">关于 </a>
                     </div>
                </div>
            </div>
        </div>  
    </div>    
    <div class="container">
        <div class="row" id = "subNavBar" style="">
            <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12">
                <div id="lblTableTitleText" style=""> 
                  <span class="glyphicon glyphicon-arrow-right" style="margin-right: 3px;"></span>
                  <span style="font-size: 16px;"> 实验列表 </span></div>
            </div>
         </div>
     </div>    


    <div class="container" id= "mainContainer" style="">
        <div class="row"  id = "row_container" style="margin-top: 20px; ">
            <div class="col-xs-4 col-sm-4 col-md-4 col-md-lg-4" style="margin-top: 50px;text-align:center;">
                <div class="tileContainer" style=""  onclick = "startProductionBalance()">
                    <!-- <img class="tileImg" src="/images/balance.png" style="width:50%">  -->
                    <div class="tileText" style=""> 
                        <!-- <span class="icon-play"></span> -->
                        <span>生产平衡实验 </span>
                        <div class="tileSubText" style="">生产平衡实验 </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-4 col-sm-4 col-md-4 col-md-lg-4" style="margin-top: 50px;text-align:center;">
                <div class="tileContainer"  style="" onclick = "startQuanlifiedProduction()">
                    <!-- <img class="tileImg" src="/images/balance.png" style="width:50%">  -->
                    <div class="tileText" style=""> 
                        良品率实验
                        <div class="tileSubText" style="">良品率实验 </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-4 col-sm-4 col-md-4 col-md-lg-4" style="margin-top: 50px;text-align:center;">
                <div  class="tileContainer" style="">
                    <!-- <img class="tileImg" src="/images/balance.png" style="width:50%">  -->
                    <div class="tileText" style="" onclick = "startProductionEfficient()"> 
                        生产效率实验
                        <div class="tileSubText" style="">生产效率实验 </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12" style="margin-top: 80px;text-align:center;margin-bottom: 20px;">
<!--                 <button type="button" class="btnAction" style="" onclick = "startProductionBalance()">生产平衡实验</button>
                <button type="button" class="btnAction" style="" onclick = "startQuanlifiedProduction()">良品率实验</button>
                <button type="button" class="btnAction" style="" onclick = "startProductionEfficient()">生产效率实验</button>
 -->        
            </div>
        </div>
  
    </div>

    <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12" id = "tipContainer" style="">
            <div> 
                <!-- <span class="glyphicon glyphicon-pushpin" style="margin-right: 3px;"></span> -->
                <span class="icon-pushpin" style="margin-right: 3px;"></span>
                <span style=""> 选择开始的实验，可以从任意一个开始 </span>
            </div>
          </div>
        </div>
    </div>  
<!-- Modal -->
    <script language="javascript" type="text/javascript">
        function openAboutWindow(){
          $('#modalAbout').modal();
        }
    </script>
<div class="modal fade" id="modalAbout" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #00695c; color: white;">
        <button type="button" class="close" data-dismiss="modal"><!-- <span aria-hidden="true">&times;</span> --><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">关于</h4>
      </div>
      <div class="modal-body">
        <div style="text-align: center; font-size: 19px; margin-top: 20px;">
           {{.aboutInfo}}
        </div>
      </div>
      <div class="modal-footer" style="text-align: center;">
        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>
<!-- Modal End-->
    <script src="/bootstrap/js/bootstrap.min.js"></script>
</body>
<div id="output"></div>
</html>
  
  