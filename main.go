package main

import (
	"github.com/astaxie/beego"
	// "github.com/astaxie/beego/logs"
	// "fmt"
	_ "startbeego/routers"
)

func main() {
	// log := logs.NewLogger(10000)
	// log.EnableFuncCallDepth(true)
	// log.Trace("trace %s %s", "param1", "param2")
	// log.Debug("debug")
	// log.Info("info")
	// log.Warn("warning")
	// log.Error("error")
	// log.Critical("critical")

	// log.
	// beego.SetLogFuncCall(true)
	beego.SetStaticPath("/images", "static/images")
	beego.SetStaticPath("/bootstrap", "static/bootstrap")
	beego.SetStaticPath("/dataTable", "static/dataTable")
	beego.SetStaticPath("/javascripts", "static/javascripts")
	beego.SetStaticPath("/layer", "static/layer")
	beego.SetStaticPath("/stylesheets", "static/stylesheets")
	// beego.Error("error 错误数据")
	// beego.Alert("alert 需要注意的数据  ")
	// beego.Debug("Debug 正常重要数据的输出")
	// beego.Emergency("emergency 无需理会的数据")
	beego.Run()
}
