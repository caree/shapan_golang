package controllers

import (
	// "encoding/json"
	// "database/sql"
	"fmt"
	"github.com/astaxie/beego"
	// "strconv"
	// _ "github.com/mattn/go-sqlite3"

	// "startbeego/controllers"
)

type PlanController struct {
	beego.Controller
}
type ProductionPlan struct {
	Index            int    `json:"index"`
	Name             string `json:"name"`
	Quantity         int    `json:"quantity"`
	Completed        bool   `json:"completed"`
	ProductTypeIndex int
}

func (this ProductionPlan) toString() string {
	return fmt.Sprintf("生产计划 => Index: %d | Name: %s | Quantity: %d | Completed: %t", this.Index, this.Name, this.Quantity, this.Completed)
}
func (this ProductionPlan) splitToSinglePiecePlan() PlanList {
	plans := PlanList{}
	for {
		if this.Quantity > 0 {
			plans = append(plans, ProductionPlan{
				Name:             this.Name,
				Quantity:         1,
				Completed:        false,
				ProductTypeIndex: this.ProductTypeIndex,
			})
			this.Quantity--
		} else {
			break
		}
	}
	return plans
}
func (this *ProductionPlan) setIndex(index int) {
	this.Index = index
}

type PlanList4DataTable struct {
	Data []ProductionPlan `json:"data"`
}
type PlanList []ProductionPlan

func (this *PlanList) addNewPlan(productType ProductType, quantity int) ProductionPlan {
	index := len(*this) + 1
	p := ProductionPlan{
		Index:            index,
		Name:             productType.Name,
		Quantity:         quantity,
		Completed:        false,
		ProductTypeIndex: productType.Index,
	}
	(*this) = append(*this, p)
	return p
}
func (this PlanList) hasUncompletedPlan() (bool, ProductionPlan) {
	for i := 0; i < len(this); i++ {
		if this[i].Completed == false {
			return true, this[i]
		}
	}
	return false, ProductionPlan{}
}

func (this *PlanList) splitToSinglePiecePlan() PlanList {
	plans := PlanList{}
	length := len(*this)
	for i := 0; i < length; i++ {
		plansTemp := (*this)[i].splitToSinglePiecePlan()
		for j := 0; j < len(plansTemp); j++ {
			plans = append(plans, plansTemp[j])
		}
	}
	for i := 0; i < len(plans); i++ {
		plans[i].setIndex(i + 1)
	}
	return plans
}
func (this PlanList) printSelf() string {
	str := ""
	for i := 0; i < len(this); i++ {
		str += "\r\n" + this[i].toString()
		// beego.Emergency(this[i].toString())
	}
	return str
}

func init() {
	// // from db
	// if rows, err := db.Query("SELECT * FROM productPlan"); err == nil {
	// 	for rows.Next() {
	// 		var Name string
	// 		var PlanIndex, Quantity, ProductTypeIndex int
	// 		var Completed bool
	// 		if rows.Scan(&PlanIndex, &Name, &Quantity, &Completed, &ProductTypeIndex) == nil {
	// 			g_planList4ProductionBalance = append(g_planList4ProductionBalance, ProductionPlan{PlanIndex, Name, Quantity, Completed, ProductTypeIndex})
	// 		}
	// 	}
	// }
	// beego.Trace("plan init...")
	// g_planList4ProductionBalance.printSelf()
}
func (this *PlanController) PlanConfigIndex() {
	this.TplNames = "planConfigIndex.tpl"
}

func (this *PlanController) AddPlanIndex() {
	this.TplNames = "addPlanIndex.tpl"
}

// func (this *PlanController) DeletePlan() {
// 	//delete form db 	then from temp
// 	beego.Trace("DeletePlan =>")
// 	beego.Trace(string(this.Ctx.Input.RequestBody))
// 	planIndex := this.GetString("index")
// 	if intPlanIndex, err := strconv.Atoi(planIndex); err == nil {
// 		if stmt, err1 := db.Prepare("delete from productPlan where PlanIndex=?"); err1 == nil {
// 			if _, err2 := stmt.Exec(intPlanIndex); err2 == nil {
// 				g_planList4ProductionBalance.removePlanByIndex(intPlanIndex)
// 				this.Data["json"] = command{Code: 0, Message: ""}
// 				goto ServeJson
// 			}
// 		}
// 	}
// 	this.Data["json"] = command{Code: 1, Message: "删除计划出现异常"}
// ServeJson:
// 	this.ServeJson()
// }
// func (this *PlanController) AddPlan() {
// 	beego.Trace("AddPlan =>")
// 	beego.Trace(string(this.Ctx.Input.RequestBody))
// 	productTypeIndex := this.GetString("productTypeIndex")
// 	quantity := this.GetString("quantity")
// 	intProductTypeIndex, err1 := strconv.Atoi(productTypeIndex)
// 	intQuantity, err2 := strconv.Atoi(quantity)
// 	if err2 != nil || err1 != nil {
// 		this.Data["json"] = command{Code: 1, Message: "添加计划出现异常！"}
// 	} else {
// 		if b, msg := addNewPlan(intProductTypeIndex, intQuantity); b {
// 			this.Data["json"] = command{Code: 0, Message: ""}
// 		} else {
// 			this.Data["json"] = command{Code: 1, Message: msg}
// 		}
// 	}
// 	this.ServeJson()
// }

// func addNewPlan(index int, quantity int) (bool, string) {
// 	count := len(g_productTypeList)
// 	for i := 0; i < count; i++ {
// 		if g_productTypeList[i].Index == index {
// 			productInfo := g_productTypeList[i]
// 			newPlan := ProductionPlan{Index: 1, Name: productInfo.Name, Quantity: quantity, Completed: false, ProductTypeIndex: productInfo.Index}
// 			if b, planTemp := g_planList4ProductionBalance.addNewPlan(newPlan); b {
// 				//db
// 				if stmt, err := db.Prepare("INSERT INTO productPlan(PlanIndex, Name, Quantity, Completed, ProductTypeIndex) values(?,?,?,?,?)"); err == nil {
// 					if _, err = stmt.Exec(planTemp.Index, planTemp.Name, planTemp.Quantity, planTemp.Completed, planTemp.ProductTypeIndex); err == nil {
// 						beego.Trace("New Plan Added")
// 						beego.Trace(planTemp)
// 						return true, ""
// 					} else {
// 						g_planList4ProductionBalance.removePlan(planTemp)
// 						beego.Error(err)
// 					}
// 				} else {
// 					g_planList4ProductionBalance.removePlan(planTemp)
// 					beego.Error(err)
// 				}

// 			}
// 		}
// 	}
// 	return false, "添加计划出现异常！"
// }
func (this PlanList) countUncompleted() int {
	count := 0
	for i := 0; i < len(this); i++ {
		if this[i].Completed == false {
			count++
		}
	}
	return count
}
func (this PlanList) completeCurrentPlan() {
	for i := 0; i < len(this); i++ {
		if this[i].Completed == false {
			(&(this[i])).Completed = true
			beego.Alert(fmt.Sprintf("完成一个生产计划, 编号为 %d %s", this[i].Index, getFileLocation()))
			beego.Emergency(this.printSelf())
			break
		}
	}
}

// func (this PlanList) addNewPlan(_plan ProductionPlan) (bool, ProductionPlan) {
// 	maxIndex := 1
// 	count := len(g_planList4ProductionBalance)
// 	for i := 0; i < count; i++ {
// 		if g_planList4ProductionBalance[i].Index > maxIndex {
// 			maxIndex = g_planList4ProductionBalance[i].Index
// 		}
// 	}
// 	_plan.Index = maxIndex + 1
// 	g_planList4ProductionBalance = append(g_planList4ProductionBalance, _plan)
// 	return true, _plan
// }
// func (this PlanList) removePlan(_plan ProductionPlan) bool {
// 	var planListTemp = PlanList{}
// 	count := len(g_planList4ProductionBalance)
// 	for i := 0; i < count; i++ {
// 		if g_planList4ProductionBalance[i].Index != _plan.Index {
// 			planListTemp = append(planListTemp, g_planList4ProductionBalance[i])
// 		}
// 	}
// 	g_planList4ProductionBalance = planListTemp
// 	return true
// }
// func (this PlanList) removePlanByIndex(_planIndex int) bool {
// 	var planListTemp = PlanList{}
// 	count := len(g_planList4ProductionBalance)
// 	for i := 0; i < count; i++ {
// 		if g_planList4ProductionBalance[i].Index != _planIndex {
// 			planListTemp = append(planListTemp, g_planList4ProductionBalance[i])
// 		}
// 	}
// 	g_planList4ProductionBalance = planListTemp
// 	return true
// }
