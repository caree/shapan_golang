package controllers

import (
	"encoding/json"
	// "fmt"
	"github.com/astaxie/beego"
	"math/rand"
)

var aboutInfo string = "精益生产沙盘V0.8"
var TimeLineRequestURI_Prefix = "timeline"

type MainController struct {
	beego.Controller
}

func init() {
	// fmt.Println("default init")
	initSayingStore()
}
func (this *MainController) Get() {
	this.Data["Website"] = "beego.me"
	this.Data["Email"] = "astaxie@gmail.com"
	this.TplNames = "index.tpl"
}

func (this *MainController) Index() {
	this.Data["aboutInfo"] = aboutInfo
	this.TplNames = "main.tpl"
	// this.LayoutSections = make(map[string]string)
	// this.LayoutSections["modal"] = "modal.tpl"
}
func (this *MainController) StartGameIndex() {
	restartGame()
	this.Data["aboutInfo"] = aboutInfo
	this.TplNames = "StartGameIndex.tpl"
}

func (this *MainController) GetRandomSaying() {
	saying := sayingsStore[rand.Intn(len(sayingsStore))]
	// fmt.Println(saying)
	beego.Trace(saying)
	// beego.Debug("this is debug")
	// beego.Info("this is info")
	// beego.Warn("this is warn")
	// beego.Error("this is error")

	b, err := json.Marshal(saying)
	// fmt.Println(string(b))
	if err != nil {
		this.Data["json"] = ""
	} else {
		this.Data["json"] = string(b)
	}
	this.ServeJson()
}

type GreatSaying struct {
	Index   int
	Author  string
	Content string
}

var sayingsStore = []GreatSaying{}

func initSayingStore() {
	// fmt.Println("initSayingStore()")
	if rows, err := db.Query("SELECT * FROM greatSaying"); err == nil {
		if rows.Next() == false {
			// fmt.Println("saying db is empty, need to initialize")
			beego.Alert("saying db is empty, need to initialize")
			// count := len(sayingsStore)
			// for i := 0; i < count; i++ {
			// 	if stmt, err := db.Prepare("INSERT INTO greatSaying(author, content) values(?,?)"); err == nil {
			// 		saying := sayingsStore[i]
			// 		if _, err := stmt.Exec(saying.Author, saying.Content); err != nil {
			// 			fmt.Println("insert saying error")
			// 		}
			// 	}
			// }
		} else {
			index := 0
			for rows.Next() {
				var author string
				var content string
				if rows.Scan(&author, &content) == nil {
					// fmt.Println(index)
					// fmt.Print(" " + author)
					// fmt.Println(" " + content)
					sayingsStore = append(sayingsStore, GreatSaying{Index: index, Author: author, Content: content})
					index++
				}

			}

		}

	} else {
		// fmt.Println(err)
		beego.Error(err)
	}

}
