package controllers

import (
	// "encoding/json"
	// "database/sql"
	"fmt"
	"github.com/astaxie/beego"
	"sort"
	"strconv"
	// _ "github.com/mattn/go-sqlite3"
)

type ProcessController struct {
	beego.Controller
}
type ProductionProcess struct {
	Index    int    `json:"index"`
	Name     string `json:"name"`
	DeviceID string `json:"deviceID"`
	Note     string `json:"note"`
}

func (this ProductionProcess) toString() string {
	// return "生产流程 => Index: %d" + strconv.Itoa(this.Index) + "  Name: " + this.Name + "  DeviceID: " + this.DeviceID + "  Note: " + this.Note
	return fmt.Sprintf("生产流程 => Index: %d | Name: %s | DeviceID: %s | Note: %s", this.Index, this.Name, this.DeviceID, this.Note)
}

type ProcessList4DataTable struct {
	Data []ProductionProcess `json:"data"`
}
type ProcessList []ProductionProcess

func (this ProcessList) printSelf() string {
	str := "\r\n"
	for i := 0; i < len(this); i++ {
		str += this[i].toString() + "\r\n"
		// beego.Emergency(this[i].toString())
	}
	return str
}
func (this ProcessList) Len() int {
	return len(this)
}
func (a ProcessList) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ProcessList) Less(i, j int) bool { return a[i].Index < a[j].Index }

var processList = ProcessList{
// ProductionPlan{Name: "产品一", Quantity: 2, Completed: false, Index: 1},
// ProductionPlan{Name: "产品二", Quantity: 2, Completed: false, Index: 2},
}

func init() {
	// from db
	if rows, err := db.Query("SELECT * FROM productProcess"); err == nil {
		for rows.Next() {
			var Name, Note, DeviceID string
			var ProcessIndex int
			if rows.Scan(&ProcessIndex, &Name, &Note, &DeviceID) == nil {
				processList = append(processList, ProductionProcess{Index: ProcessIndex, Name: Name, Note: Note, DeviceID: DeviceID})
			}
		}
	}
	// beego.Emergency("initial from db")
	// beego.Debug(processList)
	hasTotalProcess, hasQCProcess := false, false
	for i := 0; i < len(processList); i++ {
		process := processList[i]
		switch process.Index {
		case 1:
			hasTotalProcess = true
		case 256:
			hasQCProcess = true
		}
	}
	if hasTotalProcess == false {
		beego.Debug("add 总流程" + getFileLocation())
		addNewProcess("总流程", "btn000", "生产全流程", 1)
	}
	if hasQCProcess == false {
		beego.Debug("add 质检流程" + getFileLocation())
		addNewProcess("质检", "btn256", "最后的环节", 256)
	}

	// beego.Emergency("process init...")
	beego.Emergency(processList.printSelf())
}
func (this *ProcessController) ProcessConfigIndex() {
	this.Data["guid"] = GetGUID()
	this.TplNames = "processConfigIndex.tpl"
}
func (this *ProcessController) AddProcessIndex() {
	this.Data["guid"] = GetGUID()
	this.TplNames = "addProcessIndex.tpl"
}
func (this *ProcessController) TimelineIndex() {
	this.Data["guid"] = GetGUID()
	this.TplNames = "timeLine.tpl"
}

func (this *ProcessController) ProcessList() {
	sort.Sort(ProcessList(processList))
	beego.Debug("ProcessList" + getFileLocation())
	beego.Emergency(processList.printSelf())
	this.Data["json"] = ProcessList4DataTable{Data: processList}
	this.ServeJson()
}
func (this *ProcessController) DeleteProcess() {
	//delete form db 	then from temp
	beego.Emergency("DeleteProcess =>" + getFileLocation())
	beego.Emergency(string(this.Ctx.Input.RequestBody))
	processIndex := this.GetString("index")
	if intProcessIndex, err := strconv.Atoi(processIndex); err == nil {
		if intProcessIndex == 1 || intProcessIndex == 256 {
			this.Data["json"] = command{Code: 1, Message: "固定流程无法删除"}
			goto ServeJson
		}
		if stmt, err1 := db.Prepare("delete from productProcess where ProcessIndex=?"); err1 == nil {
			if _, err2 := stmt.Exec(intProcessIndex); err2 == nil {
				processList.removeProcessByIndex(intProcessIndex)
				this.Data["json"] = command{Code: 0, Message: ""}
				// ResetProcessStateList() //重置状态
				goto ServeJson
			}
		}
	}
	this.Data["json"] = command{Code: 1, Message: "删除流程出现异常"}
ServeJson:
	this.ServeJson()
}
func (this *ProcessController) AddProcess() {
	beego.Emergency("AddProcess =>" + getFileLocation())
	beego.Emergency(string(this.Ctx.Input.RequestBody))
	name := this.GetString("name")
	deviceID := this.GetString("deviceID")
	note := this.GetString("note")
	index := this.GetString("index")
	if intIndex, err := strconv.Atoi(index); err == nil {
		if intIndex <= 1 || intIndex >= 256 {
			this.Data["json"] = command{Code: 1, Message: "流程排序超出范围，需要在1和256之间！"}
			goto ServeJson
		}

		if b, msg := addNewProcess(name, deviceID, note, intIndex); b {
			// ResetProcessStateList() //重置状态
			this.Data["json"] = command{Code: 0, Message: ""}
		} else {
			this.Data["json"] = command{Code: 1, Message: msg}
		}
	} else {
		this.Data["json"] = command{Code: 1, Message: "输入参数异常"}
	}
ServeJson:
	this.ServeJson()
}
func addNewProcess(name, deviceID, note string, index int) (bool, string) {
	for i := 0; i < len(processList); i++ {
		if processList[i].DeviceID == deviceID {
			return false, "该按钮已经被使用"
		}
	}
	newProcess := ProductionProcess{Index: index, Name: name, Note: note, DeviceID: deviceID}
	if b, processTemp := processList.addNewProcess(newProcess); b {
		//db
		// beego.Emergency("addNewProcess...")
		// beego.Emergency(processTemp)
		if stmt, err := db.Prepare("INSERT INTO productProcess(ProcessIndex, Name, DeviceID, Note) values(?,?,?,?)"); err == nil {
			if _, err = stmt.Exec(processTemp.Index, processTemp.Name, processTemp.DeviceID, processTemp.Note); err == nil {
				beego.Emergency("New Process Added" + getFileLocation())
				beego.Emergency(processTemp)
				return true, ""
			} else {
				processList.removeProcess(processTemp)
				beego.Error(err)
			}
		} else {
			processList.removeProcess(processTemp)
			beego.Error(err.Error() + getFileLocation())
		}
	}
	return false, "添加流程出现异常！"
}
func (this ProcessList) addNewProcess(_process ProductionProcess) (bool, ProductionProcess) {
	if _process.Index == 1 || _process.Index == 256 {
		return true, _process
	}
	ifIndexExisted := false
	count := len(processList)
	for i := 0; i < count; i++ {
		if processList[i].Index == _process.Index {
			ifIndexExisted = true
		}
	}
	if ifIndexExisted == true {
		maxIndex := 1
		for i := 0; i < count; i++ {
			if processList[i].Index > maxIndex {
				maxIndex = processList[i].Index
			}
		}
		_process.Index = maxIndex + 1
		if _process.Index > 256 {
			return false, _process
		}
	}

	processList = append(processList, _process)
	sort.Sort(ProcessList(processList))
	return true, _process
}
func (this ProcessList) removeProcess(_process ProductionProcess) bool {
	var processListTemp = ProcessList{}
	count := len(processList)
	for i := 0; i < count; i++ {
		if processList[i].Index != _process.Index {
			processListTemp = append(processListTemp, processList[i])
		}
	}
	processList = processListTemp
	return true
}
func (this ProcessList) removeProcessByIndex(_processIndex int) bool {
	var processListTemp = ProcessList{}
	count := len(processList)
	for i := 0; i < count; i++ {
		if processList[i].Index != _processIndex {
			processListTemp = append(processListTemp, processList[i])
		}
	}
	processList = processListTemp
	return true
}
