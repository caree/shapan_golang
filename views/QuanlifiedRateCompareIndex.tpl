<!DOCTYPE html>
<meta charset="utf-8" />
<title>良品率对比</title>
    <link rel="icon" 
          type="image/png" 
          href="/images/logo_pure.png">
    <link href="/bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">    
    <link rel="stylesheet" href="/bootstrap/css/font-awesome.min.css">
    <link href="/stylesheets/style.css" rel="stylesheet" media="screen">    
    <link rel='stylesheet' href='/stylesheets/c3.css' />
    <script type="text/javascript" src = "/javascripts/jquery.js"></script>
    <script type="text/javascript" src = "/javascripts/d3.js"></script>
    <script type="text/javascript" src = "/javascripts/c3.js"></script>
    <script type="text/javascript" src = "/javascripts/underscore.js"></script>

<script language="javascript" type="text/javascript">
    // var wsUri = "ws://localhost:3003/";
	var wsUri = 'ws:' + window.location.href.substring(window.location.protocol.length)+ 'ws?guid={{.guid}}';
    var chart;
    var nPieceFlowMessages = []
    var output;
    function init() {
        output = document.getElementById("output");
        $.get("../Productivities?state=3", function(data){
            nPieceFlowMessages = data
            $.get("../Productivities?state=4", function(data){
                if(data != null){
                    nPieceFlowMessages = _.union(nPieceFlowMessages, data)
                }

                setToBarChart()
            })
            // setToLineChart()
            // createWebSocket();
            // setInterval(checkConnectionStatus, 5*1000)               
        })        
    }
    function setToBarChart(){
        chart = c3.generate({
            data: {
                // columns: [data.columns],
                columns: buildBarChartColumnData(),
                type: 'bar',
                groups: [// ['循环一', '循环二', '循环三']
                ]
            },
            axis: {
                x: {
                    type: 'category',
                    // categories: categories
                    categories: ['多件流', '单件流']
                }
            },
            bar: {
                width: {
                    ratio: 0.5 // this makes bar width 50% of length between ticks
                }
                // or
                //width: 100 // this makes bar width 100px
            }
        });           
    }
    function buildBarChartColumnData(){
        if(_.size(nPieceFlowMessages) <= 0){
            console.log('columns no data')
            return [["多件流", 0.1, 0], ["单件流", 0, 0.1]]            
        }else{
            console.log('columns data => ' + _.size(nPieceFlowMessages))
            category3 = ["多件流"]
            var npieceMsg = _.filter(nPieceFlowMessages, function(msg){return msg.NpieceValue > 1})
            if(_.size(npieceMsg) <= 0){
                category3.push(0.1)
                category1.push(0)
            }else{
                var lastOne = npieceMsg[_.size(npieceMsg)-1]
                if((lastOne.OffLineProductCount + lastOne.OffLineProductCountQcNotPassed) <= 0){
                    category3.push(0.1)
                    category1.push(0)
                }else{
                    category3.push(100 * lastOne.OffLineProductCountQcNotPassed/(lastOne.OffLineProductCountQcNotPassed + lastOne.OffLineProductCount))
                    category1.push(0)
                }
            }
            category1 = ["单件流"]
            var npieceMsg = _.filter(nPieceFlowMessages, function(msg){return msg.NpieceValue <= 1})
            if(_.size(npieceMsg) <= 0){
                category1.push(0)
                category1.push(0.1)
            }else{
                var lastOne = npieceMsg[_.size(npieceMsg)-1]
                if((lastOne.OffLineProductCount + lastOne.OffLineProductCountQcNotPassed) <= 0){
                    category1.push(0)
                    category1.push(0.1)
                }else{
                    category1.push(0)
                    category1.push(100 * lastOne.OffLineProductCountQcNotPassed/(lastOne.OffLineProductCountQcNotPassed + lastOne.OffLineProductCount))
                }
            }
            console.log(JSON.stringify([category3, category1]))
            return [category3, category1]
        }
    }

    function startNextStep(){
        window.location.href="../QuanlifiedRateCompareIndex"
    }    
    function createWebSocket() {
        websocket = new WebSocket(wsUri);
        websocket.onopen = function (evt) { onOpen(evt) };
        websocket.onclose = function (evt) { onClose(evt) };
        websocket.onmessage = function (evt) { onMessage(evt) };
        websocket.onerror = function (evt) { onError(evt) };
    }
    function onOpen(evt) {
        writeToScreen("已连接到服务器");
    }
    function onClose(evt) {
        writeToScreen("连接已断开");
        websocket=null
    }
    function onMessage(evt) {
        // writeToScreen('<span style="color: blue;">RESPONSE: ' + evt.data+'</span>');
        var cmd = JSON.parse(evt.data);
        switch(cmd.Type){
            case 4:
                writeToScreen(cmd.Content)
                break
            case 6:
                console.log(cmd.Content)
                updateNPieceFlowMessage(JSON.parse(cmd.Content))
                break
            case 7:
                // setToPieChart()
                startNextStep()
            break
        }
    }
    function onError(evt) {
        writeToScreen('<span style="color: red;">ERROR:</span> ' + evt.data);
        websocket=null
    }
    function doSend(message) {
        writeToScreen("SENT: " + message);
        websocket.send(message);
    }
    function writeToScreen(message) {
        var pre = document.createElement("p");
        pre.style.wordWrap = "break-word";
        pre.innerHTML = message;
        output.appendChild(pre);
    }
    function checkConnectionStatus(){
      if(websocket == null){
        createWebSocket()
      }
    }    
    window.addEventListener("load", init, false);
</script>

<!-- <h2>N件流</h2> -->
<body> 
    <div id = "divNav">
        <div class="container">
            <div class="row"  id = "">
                <div class="col-xs-1 col-sm-1 col-md-1 col-md-lg-1">
                    <img id="imglogo" src="/images/logo3.png">
                </div>
                <div class="col-xs-11 col-sm-11 col-md-11 col-md-lg-11">
                    <div style="">
                           <a class= "navFunc" style="right:6px;" onclick = "openAboutWindow()">关于 </a>
                     </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row" id = "subNavBar" style="">
            <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12">
                <div id="lblTableTitleText" style=""> 
                  <span class="glyphicon glyphicon-arrow-right" style="margin-right: 3px;"></span>
                  <span style="font-size: 16px;"> 不良品实验结果对比统计图 </span></div>
            </div>
         </div>
     </div>   

    <div class="container" >
        <div class="row chartOutterContainer"  id = "row_container">
            <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12" style="margin-top:5px;">
                <div id="chart" style=""  class="chartInnerContainer"></div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12" style="margin-top: 30px;margin-bottom: 10px;text-align:center;">
                <button type="button" class="btnMayDo" style="width: 150px;" onclick = "setToLineChart()">不良品量统计图</button>
                <button type="button" class="btnMayDo" style="width: 150px;" onclick = "setToPieChart()">良品率统计图</button>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12" id = "tipContainer" style="">
            <div> 
                <span class="icon-pushpin" style="margin-right: 3px;"></span>
                <span style=""> 上图展示了之前良品率实验过程中多件流和单件流对良品量的影响的对比 </span>
            </div>
          </div>
        </div>
    </div>  
<!-- Modal -->
    <script language="javascript" type="text/javascript">
        function openAboutWindow(){
          $('#modalAbout').modal();
        }
    </script>
<div class="modal fade" id="modalAbout" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #00695c; color: white;">
        <button type="button" class="close" data-dismiss="modal"><!-- <span aria-hidden="true">&times;</span> --><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">关于</h4>
      </div>
      <div class="modal-body">
        <div style="text-align: center; font-size: 19px; margin-top: 20px;">
           {{.aboutInfo}}
        </div>
      </div>
      <div class="modal-footer" style="text-align: center;">
        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>
<!-- Modal End-->
    <script src="/bootstrap/js/bootstrap.min.js"></script>
</body>
<div id="output"></div>
</html>
  
  