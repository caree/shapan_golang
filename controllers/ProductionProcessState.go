package controllers

import (
	// "encoding/json"
	"fmt"
	"github.com/astaxie/beego"
	// "startbeego/models"
	"strconv"
	"time"
	// "math/rand"
)

type ProductionProcessState struct {
	ProductionProcess
	state      string
	startStamp int64
	endStamp   int64
	startTime  string
	endTime    string
	loopIndex  int
	// nPieceValue int
	// Index    int    `json:"index"`
	// Name     string `json:"name"`
	// DeviceID string `json:"deviceID"`
	// Note     string `json:"note"`
}

func (this ProductionProcessState) toString() string {
	// return fmt.Sprintf("%+v", this)
	return fmt.Sprintf("流程信息 =>  批次：%d | 环节：%s | 开始时间：%s(%d) | 停止时间：%s(%d)  ", this.loopIndex, this.Name, this.startTime, this.startStamp, this.endTime, this.endStamp)
}
func (this ProductionProcessState) completed() bool {
	if this.endStamp > 0 && this.startStamp > 0 {
		return true
	}
	return false
}
func (this ProductionProcessState) isQCPassed() bool {
	return this.isQCprocess() && this.state == "off"
}
func (this ProductionProcessState) isQCprocess() bool {
	return this.DeviceID == "btn256"
}
func (this ProductionProcessState) started() bool {
	if this.startStamp > 0 {
		return true
	}
	return false
}

// func (this *ProductionProcessState) setNpieceValue(value int) {
// 	this.nPieceValue = value
// }

func (this *ProductionProcessState) start(btnCommand BtnCommand) bool {
	t := time.Now() //获取当前时间的结构体
	this.state = "on"
	this.startTime = t.Format("2006-01-02 15:04:05")
	this.startStamp = t.Unix()

	str1 := this.Name + " 开始 " + this.startTime
	beego.Debug(str1 + getFileLocation())
	beego.Emergency(this.toString())
	newTimeLineMessage(str1).broadcastTimeLineMessage()
	return true
}

func (this *ProductionProcessState) stopForced() {
	t := time.Now() //获取当前时间的结构体
	this.endTime = t.Format("2006-01-02 15:04:05")
	this.endStamp = t.Unix()
	if this.isQCprocess() { //如果是质检流程
		this.state = "yas"
		str1 := "质检流程结束， 产品质量不合格"
		beego.Debug(str1 + getFileLocation())
		newTimeLineMessage(str1).broadcastTimeLineMessage()
	} else {
		//流程结束
		this.state = "off"
		str1 := this.Name + " 被强制结束"
		beego.Debug(str1 + getFileLocation())
		newTimeLineMessage(str1).broadcastTimeLineMessage()
	}
}
func (this *ProductionProcessState) stop(btnCommand BtnCommand) bool {
	t := time.Now() //获取当前时间的结构体
	this.endTime = t.Format("2006-01-02 15:04:05")
	this.endStamp = t.Unix()
	timeElapsed := this.endStamp - this.startStamp

	if this.DeviceID == "btn256" { //如果是质检流程
		if btnCommand.State == "off" {
			this.state = "off"
			// completeCurrentPlan()
			// str1 := "质检流程结束， 产品质量合格"
			// beego.Debug(str1)
			// newTimeLineMessage(str1).broadcastTimeLineMessage()

		} else if btnCommand.State == "yas" {
			this.state = "yas"
			// completeCurrentPlan()
			// setAllOnlineProductsUnquanlified()
			// str1 := "质检流程结束， 产品质量不合格"
			// beego.Debug(str1)
			// newTimeLineMessage(str1).broadcastTimeLineMessage()
		}
	} else {
		//流程结束
		this.state = "off"
		str1 := this.Name + " 结束  从 " + this.startTime + "  到 " + this.endTime + " 共耗时 " + strconv.Itoa(int(timeElapsed)) + " 秒"
		beego.Alert(str1 + getFileLocation())
		beego.Emergency(this.toString())
		newTimeLineMessage(str1).broadcastTimeLineMessage()
	}
	return true
}

type ProcessStateList []ProductionProcessState

func (this *ProcessStateList) addNewProcessStateListLoop() {
	loopIndex := 0
	if len(*this) > 0 {
		loopIndex = (*this)[len(*this)-1].loopIndex + 1
	}
	for i := 1; i < len(*this); i++ {
		*this = append(*this, ProductionProcessState{
			ProductionProcess: processList[i],
			state:             "off",
			startStamp:        0,
			endStamp:          0,
			startTime:         "",
			endTime:           "",
			loopIndex:         loopIndex})
	}
	beego.Emergency("addNewProcessStateListLoop =>" + getFileLocation())
	this.printSelf()
}

func (this *ProcessStateList) countCompletedProducts(loopIndex, pieceValue int) (qcPassed, qcNotPassed int) {
	if b, state := this.lastProcessInLoop(loopIndex); b {
		if state.completed() {
			if state.isQCPassed() {
				beego.Debug("循环 " + strconv.Itoa(loopIndex) + " 的质检流程 已经通过" + getFileLocation())
				return g_NpieceValue, 0
			} else {
				beego.Debug("循环 " + strconv.Itoa(loopIndex) + " 的质检流程未通过" + getFileLocation())
				return 0, g_NpieceValue
			}
		} else {
			beego.Alert("循环 " + strconv.Itoa(loopIndex) + "质检流程尚未结束，没有下线产品" + getFileLocation())
		}
	} else {
		beego.Alert("没找到质检流程" + getFileLocation())
	}
	return 0, 0
}
func (this *ProcessStateList) countUncompletedProducts(loopIndex, pieceValue int) int {
	if b, state := this.firstProcessInLoop(loopIndex); b {
		if !state.started() {
			beego.Debug("循环 " + strconv.Itoa(loopIndex) + " 的第一流程 " + state.Name + " 尚未开始" + getFileLocation())
			return 0
		} else {
			beego.Debug("循环 " + strconv.Itoa(loopIndex) + " 的第一流程 " + state.Name + " 已经开始" + getFileLocation())
		}
		if b, state = this.lastProcessInLoop(loopIndex); b {
			if !state.completed() {
				beego.Debug("循环 " + strconv.Itoa(loopIndex) + " 的最后流程 " + state.Name + " 尚未完成" + getFileLocation())
				beego.Emergency(state.toString())
				return g_NpieceValue
			} else {
				beego.Debug("循环 " + strconv.Itoa(loopIndex) + " 的最后流程 " + state.Name + " 已经完成" + getFileLocation())
			}
		}
	}

	return 0
}
func (this *ProcessStateList) lastProcessInLoop(loopIndex int) (bool, ProductionProcessState) {
	for i := 0; i < len(*this); i++ {
		process := (*this)[i]
		if process.loopIndex == loopIndex && process.isQCprocess() {
			return true, process
		}
	}
	return false, (*this)[0]
}
func (this *ProcessStateList) firstProcessInLoop(loopIndex int) (bool, ProductionProcessState) {
	tempList := ProcessStateList{}
	for i := 0; i < len(*this); i++ {
		if (*this)[i].loopIndex == loopIndex {
			tempList = append(tempList, (*this)[i])
		}
	}
	if len(tempList) <= 0 {
		return false, (*this)[0]
	} else {
		return true, tempList[0]
	}
}
func (this *ProcessStateList) printSelf() string {
	str := ""
	for i := 0; i < len(*this); i++ {
		str += "\r\n" + (*this)[i].toString()
		// beego.Emergency()
	}
	return str
}

func (this ProcessStateList) findProcessByDeviceID(deviceID string) (bool, *ProductionProcessState) {
	// fmt.Println("the length: " + strconv.Itoa(len(this)))
	// return false, &(this[0])
	for i := 0; i < len(this); i++ {
		if this[i].DeviceID == deviceID && this[i].completed() == false {
			return true, &(this[i])
		}
	}
	return false, &(this[0])
}

func (this ProcessStateList) checkTotalProcess(btnCommand BtnCommand) bool {
	if btnCommand.BtnName == totalProcess.DeviceID {
		if btnCommand.open() {
			if totalProcess.started() {
				beego.Alert("总流程已经开始" + getFileLocation())
				// return false
			} else {
				//todo 清空之前的记录

				totalProcess.start(btnCommand)
				// beego.Info("总流程开始")
				// return false
			}
		} else { // off
			if totalProcess.completed() {
				beego.Alert("总流程已经结束" + getFileLocation())
				// return false
			}
			if !totalProcess.started() {
				beego.Alert("总流程尚未开始" + getFileLocation())
				// return false
			} else {
				//总流程结束
				totalProcess.stop(btnCommand)
				// return false
			}
		}
		return false
	}
	return true
}

func (this ProcessStateList) isFirstProcess(state *ProductionProcessState) bool {
	if len(this) <= 1 {
		return false
	}
	//在每一个loop里面排第一位的就是第一个流程
	for i := 0; i < len(this); i++ {
		if this[i].loopIndex == state.loopIndex {
			return this[i].Index == state.Index
		}
	}
	return false
}
func (this ProcessStateList) findUpperProcess(state *ProductionProcessState) (bool, ProductionProcessState) {
	for i := 0; i < len(this); i++ {
		if this[i].Index == state.Index && this[i].loopIndex == state.loopIndex {
			// beego.Debug("查找到合适的流程，i = " + strconv.Itoa(i))
			if i <= 0 {
				return false, this[0]
			}
			return true, this[i-1]
		}
	}

	return false, this[0]
}
