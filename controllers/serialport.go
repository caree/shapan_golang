package controllers

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/astaxie/beego"
	"github.com/tarm/goserial"
	"io"
	"log"
	"os/exec"
	"regexp"
	"startbeego/models"
	"strings"
	"time"
)

type BtnInfoHandler func(data string)
type BtnInfoHandlerInfo struct {
	Handler BtnInfoHandler
	Name    string
}
type BtnCommand struct {
	BtnName string `json:btnName`
	State   string `json: state`
}

func (this BtnCommand) closeYAS() bool {
	if this.State == "yas" {
		return true
	} else {
		return false
	}

}
func (this BtnCommand) closeNormal() bool {
	if this.State == "off" {
		return true
	} else {
		return false
	}
}
func (this BtnCommand) open() bool {
	if this.State == "on" {
		return true
	} else {
		return false
	}
}

type BtnInfo struct {
	Cs       chan string
	PortName string
	Handlers []BtnInfoHandlerInfo
	rwc      io.ReadWriteCloser
}
type BtnInfoList []BtnInfo

var btnInfoList BtnInfoList

func init() {
	btnInfoList = BtnInfoList{}
	openSerialPortList()
}
func openSerialPortList() {
	cmd := exec.Command("ls", "/dev/")
	var out bytes.Buffer
	cmd.Stdout = &out
	err := cmd.Run()
	if err != nil {
		fmt.Println(err)
		return
	}
	devListString := out.String()
	// fmt.Printf(devListString)
	devList := strings.Split(devListString, "\n")
	// fmt.Println(devList)
	count := len(devList)
	for i := 0; i < count; i++ {
		if strings.HasPrefix(devList[i], "tty.usb") || strings.HasPrefix(devList[i], "tty.wch") {
			fmt.Println(devList[i])
			cs := make(chan string)
			btnInfo := BtnInfo{Cs: cs, PortName: devList[i]}
			go startSerialPort(&btnInfo)
			// go startSerialPort(devList[i], cs)
			go portListening(btnInfo)
			btnInfoList = append(btnInfoList, btnInfo)
		}
	}
}
func closeSerialPortList() {
	for i := 0; i < len(btnInfoList); i++ {
		if btn := btnInfoList[i]; btn.rwc != nil {
			if err := btn.rwc.Close(); err != nil {
				beego.Error("Close serial error")
			}
		}
	}
	btnInfoList = BtnInfoList{}
}

// func (this BtnInfoList) AddBtnHandler(handlerInfo BtnInfoHandlerInfo) {
// 	if len(this) <= 0 {
// 		return
// 	}
// 	existed := false
// 	for i := 0; i < len(this[0].Handlers); i++ {
// 		if this[0].Handlers[i].Name == handlerInfo.Name {
// 			existed = true
// 			break
// 		}
// 	}
// 	if !existed {
// 		for i := 0; i < len(this); i++ {
// 			this[i].Handlers = append(this[i].Handlers, handlerInfo)
// 		}
// 	}
// }
func portListening(btnInfo BtnInfo) {
	t := time.Now() //.UnixNano()
	// fmt.Println(t)
	for {
		str := <-btnInfo.Cs
		t1 := time.Now()
		fmt.Println(t1.Sub(t).Nanoseconds() / 1000000)
		t = t1
		// fmt.Println("btn info: " + str)
		re := regexp.MustCompile("\\[btn[0-9]{3,},[a-z]{2,3}\\]")
		str = re.FindString(str)
		// fmt.Println("btn info reg: " + str)
		// btnCommand := BtnCommand{BtnName: str[2:4], State: strings.Trim(str[7:9], " ]")}
		btnCommand := BtnCommand{BtnName: str[1:7], State: strings.Trim(str[8:11], " ]")}
		// beego.Warn(btnCommand)
		// return
		updateProcessState(btnCommand)
		data, err := json.Marshal(btnCommand)
		if err != nil {
			beego.Error("Fail to marshal btnCommand:", err)
			return
		}
		broadcastWebSocket(newEvent(models.EVENT_BUTTON, "all", string(data)), "")
	}
}
func startSerialPort(btnInfo *BtnInfo) {
	// func startSerialPort(shortName string, cs chan string) {
	c := &serial.Config{Name: "/dev/" + btnInfo.PortName, Baud: 9600}
	s, err := serial.OpenPort(c)
	if err != nil {
		log.Fatal(err)
	}
	btnInfo.rwc = s
	str := ""
	for {
		buf := make([]byte, 128)
		n, err := s.Read(buf)
		if err != nil {
			log.Fatal(err)
		}
		// log.Print(string(buf[:n]))
		str += string(buf[:n])
		// fmt.Println(str)
		tailSepIndex := strings.Index(str, "]")
		if tailSepIndex > 0 {
			headSepIndex := strings.Index(str, "[")
			if headSepIndex >= 0 {
				data := str[headSepIndex : tailSepIndex+1]
				fmt.Println(btnInfo.PortName + " => " + data)
				btnInfo.Cs <- data
			}
			str = ""
		} else {
			// fmt.Println("no suffix ]")
		}
	}

}

// n, err := s.Write([]byte("test"))
// if err != nil {
//         log.Fatal(err)
// }
