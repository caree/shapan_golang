<!DOCTYPE html>
<meta charset="utf-8" />
<title>生产平衡实验</title>
    <link rel="icon" 
          type="image/png" 
          href="/images/logo_pure.png">
    <link href="/bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">    
    <link rel="stylesheet" href="/bootstrap/css/font-awesome.min.css">
    <link rel='stylesheet' href='/stylesheets/c3.css' />
    <link href="/stylesheets/style.css" rel="stylesheet" media="screen">    
    <script type="text/javascript" src = "/javascripts/jquery.js"></script>
    <script type="text/javascript" src = "/javascripts/d3.js"></script>
    <script type="text/javascript" src = "/javascripts/c3.js"></script>
    <script type="text/javascript" src = "/javascripts/underscore.js"></script>
 
<script language="javascript" type="text/javascript">
    // var wsUri = "ws://localhost:3003/";
	var wsUri = 'ws:' + window.location.href.substring(window.location.protocol.length)+ 'ws?guid={{.guid}}';
    var chart;
    var output;
    var columns = []
    var nullColumn = null
    var chartStyle = "null"
    var categories
    function setToBarChart(){
        if(chartStyle == 'bar'){
            return
        }
        chartStyle = 'bar'
        chart = c3.generate({
            data: {
                // columns: [data.columns],
                x: "x",
                columns: buildBarChartColumnData(),
                type: 'bar',
                groups: [// ['循环一', '循环二', '循环三']
                ],
                labels: {
                       // format: function (v, id) { return v + "S"; },
                }
            },
            bar: {
                // width: 500 // this makes bar width 100px
                ratio: 0.1
            },
            axis: {
                x: {
                    type: 'category',
                    // categories: categories
                    // categories: ['环节一', '环节二', '环节三']
                },
                y: {
                    label: {
                        text: '时间(秒)',
                        position: 'outer-middle',
                    }
                },
            },
            grid: {
                    x: {
                        show: false
                    },
                    y: {
                        show: true
                    }
            }            
        });   
    }
    function setToPieChart(){
        if(chartStyle == 'pie'){
            return
        }
        chartStyle = 'pie'
        chart = c3.generate({
            data: {
                // iris data from R
                columns: buildPieChartColumnData(),
                type : 'pie',
            }
        });
    }
    function Column(name, categories, values){
        var column = this;
        column.name = name
        column.values = []
        if(values == null){
            _.each(categories, function(_category){
                column.values.push({name: _category, value: 10})
            })
        }else{
            _.each(categories, function(_category, _index){
                column.values.push({name: _category, value: values[_index]})
            })            
        }
    }
    Column.prototype.updateTimeElapse = function(data){
        // var valueTemp = _.findWhere(this.values, {name: data.name})
        // if(valueTemp == null){
        //     this.values.push({name: data.name, value: data.timeElapse})
        // }else{
        //     valueTemp.value = data.timeElapse
        // }
        _.each(this.values, function(_value){
            if(_value.name == data.name){
                _value.value = data.timeElapse
            }
        })
    }
    Column.prototype.toColumnData = function() {
        var column = this
        var result = [column.name]
        _.each(column.values, function(_value){
            result.push(_value.value)
        })
        console.log("toColumnData => "+ JSON.stringify(result))
        return result
    };
    // var columns = [{name: '', values: [0.01, 0.01]}]
    // var columns = [['生产批次0', 0.01, 0.01]]

    function buildBarChartColumnData(){
        if(nullColumn == null) return
        var data = []
        var xCatigories = _.union(["x"], categories)
        if(_.size(columns) <= 0){
            console.log('columns no data')
            data = [xCatigories, nullColumn.toColumnData()]            
        }else{
            console.log('columns data => ' + _.size(columns))
            var columnDatas = _.map(columns, function(_column){
                return _column.toColumnData()
            })           
            // data = [xCatigories, columnDatas] 
            data = _.union([xCatigories], columnDatas)
        }
        console.log('buildBarChartColumnData => ' + JSON.stringify(data))
        return data
    }
    function buildPieChartColumnData(){
        if(nullColumn == null) return
        if(_.size(columns) <= 0){
            console.log('columns no data')
            return _.map(nullColumn.values, function(_value){
                return [_value.name, _value.value]
            })            
        }else{
            console.log('columns data => ' + _.size(columns))         
            var allValues = _.reduce(columns, function(memo, _column){
                _.each(_column.values, function(_value){
                    var tempValue = _.find(memo, function(_subMemo){
                        return _subMemo[0] == _value.name
                        // return _subMemo.name == _value.name
                    })
                    if(tempValue == null){
                        memo.push([_value.name, _value.value])
                        // memo.push(_value)
                    }else{
                        tempValue.push(_value.value)
                    }
                })
                return memo
            }, [])
            console.dir(allValues)
            return allValues
        }
    }
    function caculateBalanceRate(){
        if(nullColumn == null) return 0
        if(_.size(columns) <= 0){
            console.log('caculateBalanceRate no data')
            var timeElapses = _.pluck(nullColumn.values, "value")
            // console.log(timeElapses)
            var plusResult = _.reduce(timeElapses, function(memo, _value){return memo + _value }, 0)
            console.log('plusResult => ' + plusResult)
            var rtnValue = 100 * plusResult /(_.max(timeElapses) * _.size(timeElapses))
            console.log('return => ' + rtnValue)
            return rtnValue
        }else{
            console.log('columns data => ' + _.size(columns))         
            var allValues = _.reduce(columns, function(memo, _column){
                _.each(_column.values, function(_value){
                    var tempValue = _.find(memo, function(_subMemo){
                        return _subMemo[0] == _value.name
                        // return _subMemo.name == _value.name
                    })
                    if(tempValue == null){
                        memo.push([_value.name, _value.value])
                        // memo.push(_value)
                    }else{
                        // tempValue.push(_value.value)
                        tempValue[1] = tempValue[1] + _value.value
                    }
                })
                return memo
            }, [])
            console.dir(allValues)
            var maxValue = _.max(allValues, function(_value){return _value[1]})
            console.log('maxValue => ' + maxValue[1])
            var plusResult = _.reduce(allValues, function(memo, _value){return memo + _value[1]}, 0)
            console.log('plusResult => ' + plusResult)
            return 100 * plusResult/(maxValue[1]*_.size(allValues))
            // return allValues
        }
    }
    function addColumn(loopDatas){
        if(_.isArray(loopDatas) == false || _.size(loopDatas) <= 0) return
        // [{"loopIndex":0,"loopName":"生产批次0","index":2,"name":"流程一","timeElapse":1},{"loopIndex":0,"loopName":"生产批次0","index":3,"name":"流程二","timeElapse":1}] 
        var column = _.findWhere(columns, {name: loopDatas[0].loopName})
        if(null == column){
            // var categories = _.pluck(loopDatas, 'name');
            var values = _.pluck(loopDatas, 'timeElapse');
            while(_.size(values) < _.size(categories)){
                values.push(0)
            }
            columns.push(new Column(loopDatas[0].loopName, categories, values))
        }else{
            _.each(loopDatas, function(data){
                column.updateTimeElapse(data)
            })
            // column.values.push({name: data.name, value: data.timeElapse})
        }
        // console.log("columns: =>")
        // console.log(columns)
        var c = buildBarChartColumnData()
        if(chartStyle == 'pie'){
            c = buildPieChartColumnData()
        }else if(chartStyle == 'null'){
            return
        }
        console.log('load chart =>')
        console.log(JSON.stringify(c))
        chart.load({
            unload: ['demo'],
            columns: c,
        });
        chart.flush()
        chart.legend.show()
        setBalanceRate(caculateBalanceRate())
    }
    function init() {
        output = document.getElementById("output");

        $.get('../processList', function(data){
            data = data.data;
            // console.dir(data);
            categories = _.without(_.map(data, function(_cat){
                            if(_cat.index != 1 && _cat.index != 256)
                                return _cat.name;
                            else return null;
                        }), null);
            console.log("categories => "+ JSON.stringify(categories))
            // categories = _.without(categories, null);
            nullColumn = new Column('demo', categories)
            // initialColumns(categories)
            // return
            setToBarChart()
            chart.legend.hide()
            // setToPieChart()
            setBalanceRate(caculateBalanceRate())
        })  
        createWebSocket()
        setInterval(checkConnectionStatus, 5000)
        // return
        // setTimeout(function () {
        //     setBalanceRate(80)
        // }, 1500);
        // setTimeout(function () {
        //     chart = c3.generate({
        //         data: {
        //             // iris data from R
        //             columns: [
        //                 ['data1', 30],
        //                 ['data2', 120],
        //             ],
        //             type : 'pie',
        //         }
        //     });
        // }, 1500);
    }
    function setBalanceRate(value){
        console.log('setBalanceRate => ' + value)
        if(_.isNumber(value) == false) return
        value = value.toFixed()
        var divBalanceRate = $('#divBalanceRate')
        if(value <= 70){
            divBalanceRate.css('color', "rgb(128,0,0)")
        }else if(value > 70 && value < 93){
            divBalanceRate.css('color', "rgb(255,128,0)")
        }else{
            divBalanceRate.css('color', "rgb(0,128,0)")
        }
        divBalanceRate.text(value + "%")
        return
        // var progressbar = $('#balanceRateBar')
        // if(value <= 70){
        //     progressbar.css('background-color', "rgb(128,0,0)")
        // }else if(value > 70 && value < 93){
        //     progressbar.css('background-color', "rgb(255,128,0)")
        // }else{
        //     progressbar.css('background-color', "rgb(0,128,0)")
        // }
        // progressbar.css('width', value+"%")
    }
    function onMessage(evt) {
        // writeToScreen('<span style="color: blue;">RESPONSE: ' + evt.data+'</span>');
        var cmd = JSON.parse(evt.data);
        // console.log(cmd)
        switch(cmd.Type){
            case 4:
                writeToScreen(cmd.Content)
                break;
            case 5:
                console.log(cmd.Content)
                var data = JSON.parse(cmd.Content)
                console.log(data)
                // console.log(data.Result.timeElapse)
                addColumn(data)
                break
            case 7:
                console.log('生产计划已经完成')
                // setToPieChart()
            break
        }
        // if(cmd.Type==4){
        //     writeToScreen(cmd.Content)
        // }
    }
    function createWebSocket() {
        websocket = new WebSocket(wsUri);
        websocket.onopen = function (evt) { onOpen(evt) };
        websocket.onclose = function (evt) { onClose(evt) };
        websocket.onmessage = function (evt) { onMessage(evt) };
        websocket.onerror = function (evt) { onError(evt) };
    }
    function onOpen(evt) {
        writeToScreen("已连接到服务器");
    }
    function onClose(evt) {
        writeToScreen("连接已断开");
        websocket=null
    }
    function onError(evt) {
        writeToScreen('<span style="color: red;">ERROR:</span> ' + evt.data);
        websocket=null
    }
    function doSend(message) {
        writeToScreen("SENT: " + message);
        websocket.send(message);
    }
    function writeToScreen(message) {
        return
        var pre = document.createElement("p");
        pre.style.wordWrap = "break-word";
        pre.innerHTML = message;
        output.appendChild(pre);
    }
    function checkConnectionStatus(){
      if(websocket == null){
        createWebSocket()
      }
    }    
    window.addEventListener("load", init, false);
</script>

<!-- <h2>N件流</h2> -->
<body> 
    <div id = "divNav">
        <div class="container">
            <div class="row"  id = "">
                <div class="col-xs-1 col-sm-1 col-md-1 col-md-lg-1">

                    <img id="imglogo" src="/images/logo3.png">
                </div>
                <div class="col-xs-11 col-sm-11 col-md-11 col-md-lg-11">
                    <div style="">
                           <a class= "navFunc" style="right:6px;" onclick = "openAboutWindow()">关于 </a>
                     </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row" id = "subNavBar" style="">
            <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12">
                <div id="lblTableTitleText" style=""> 
                  <span class="glyphicon glyphicon-arrow-right" style="margin-right: 3px;"></span>
                  <span style="font-size: 16px;"> 生产平衡实验统计图 </span></div>
            </div>
         </div>
     </div>    

    <div class="container" id= "">
        <div class="row chartOutterContainer"  id = "row_container">
            <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12 " style="margin-top:75px;">
                <div style="text-align: center; text-indent: -220px; color: rgba(150,150,150,0.8);">当前平衡率： </div>
                <div id="divBalanceRate" style="text-align: center; font-size: 60px; margin-top: -66px;"> 100%</div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12 " style="">
                <div id="chart" style="" class="chartInnerContainer"></div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12" style="margin-top: 0px;margin-bottom: 20px;text-align:center;">
                <div class="icon-spinner icon-spin icon-2x pull-left" style="width:100%"> </div>
                <div class="" style="text-align: center; color: rgba(150,150,150,0.8);margin-top: 40px;" > 数据接收中...</div>
<!--                 <button type="button" class="btnMayDo" style="" onclick = "setToBarChart()">柱状统计图</button>
                <button type="button" class="btnMayDo" style="" onclick = "setToPieChart()">饼状比重图</button>
 -->            
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12" id = "tipContainer" style="">
            <div> 
                <span class="icon-pushpin" style="margin-right: 3px;"></span>
                <span style=""> 上图展示了实验过程中的每个流程所耗费的时间，以及由此产生的平衡率 </span>
            </div>
          </div>
        </div>
    </div>  
    <!-- 滚动条 -->
<!--     <div class="container" id= "mainContainer">
        <div class="row"  id = "row_container">

            <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12" style="margin-top:5px;">
                <div class="progress">
                  <div id="balanceRateBar" class="progress-bar progress-bar-striped active"  role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%">
                    <span class="sr-only">45% Complete</span>
                  </div>
                </div>
                
            </div>
        </div>        
    </div> -->

<!-- Modal -->
    <script language="javascript" type="text/javascript">
        function openAboutWindow(){
          $('#modalAbout').modal();
        }
    </script>
<div class="modal fade" id="modalAbout" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #00695c; color: white;">
        <button type="button" class="close" data-dismiss="modal"><!-- <span aria-hidden="true">&times;</span> --><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">关于</h4>
      </div>
      <div class="modal-body">
        <div style="text-align: center; font-size: 19px; margin-top: 20px;">
           {{.aboutInfo}}
        </div>
      </div>
      <div class="modal-footer" style="text-align: center;">
        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>
<!-- Modal End-->
    <div id="output"></div>
    <script src="/bootstrap/js/bootstrap.min.js"></script>
</body>


</html>
  
  