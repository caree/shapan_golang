<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>生产流程设置</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" 
          type="image/png" 
          href="/images/logo_pure.png">
    <!-- Loading Bootstrap -->
    <!-- <link href="bootstrap/css/bootstrap.css" rel="stylesheet"> -->
    <link href="/bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">    
    <link href="/stylesheets/style.css" rel="stylesheet" media="screen">    
    <link href="/dataTable/jquery.dataTables.css" rel="stylesheet" media="screen">    
    <style type="text/css">

      #lblTip{
        margin-top: 30px; text-align: center; font-size: 30px; color: red; margin-bottom: 20px;
      }
      #dtProcess{
        text-align: center;
      }
      th{
        text-align: center;
      }
      .btn{
        width: 100px;
      }
          td.highlight {
              background-color: rgba(0,256,0, 0.4) !important;
          }
          tr.highlight{
              background-color: rgba(0,256,0, 0.4) !important;

          }
    </style>
    <!-- // <script data-main="javascripts/main" src="/javascripts/require.js"></script> -->
    <script type="text/javascript" src = "/javascripts/jquery.js"></script>
    <script type="text/javascript" src = "/javascripts/underscore.js"></script>
    <script type="text/javascript">
        var wsUri = 'ws:' + window.location.href.substring(window.location.protocol.length) + 'ws?guid={{.guid}}';
        var cmds = [];
        var table;

        $(document).ready(function() {
            table = $('#dtProcess').DataTable( {
                "paging":   false,
                "ordering": false,
                "info":     false,
                "searching":  false,
                "ajax": "../processList",
                "columns": [
                    { "data": "index" ,"width": "25%"},
                    { "data": "name" ,"width": "25%"},
                    { "data": "deviceID" ,"width": "25%"},
                    { "data": "note" ,"width": "25%"},
                ]
            } );
            table.on('draw', function(){
                return;
                var rowsCount = table.rows()[0].length;
                for(var i = 0; i< rowsCount; i++){
                    var data = table.row().nodes().data()[i];
                    console.log(data.deviceID);
                    if(data.deviceID == 'btn001'){

                      $(table.cell(i,2).node()).addClass( 'highlight' );
                    }
                }
                // $( table.row(0).node()).addClass( 'highlight' );
                // $( table.column(2).nodes() ).addClass( 'highlight' );
                // $(table.cell(1,1).node()).addClass( 'highlight' );
            })

            $('#dtProcess tbody').on( 'click', 'tr', function () {
                if ( $(this).hasClass('selected') ) {
                    $(this).removeClass('selected');
                }
                else {
                    table.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                }
            } );
            // $.get('../processList', function(data){
            //   var processes = JSON.parse(data);

            // })
            // hideTip();

            // updateSaying();
            // setInterval(updateSaying, 30 * 1000);

            createWebSocket();
            setInterval(checkConnectionStatus, 5*1000)
        });
        function updateSaying(){
            $.get('../getRandomSaying', function(_data){
                var saying = JSON.parse(_data);
                $('#sayingContent').text(saying.content);
                $('#sayingAuthor').text(saying.author);
            })
        }
        function openAboutWindow(){
          $('#modalAbout').modal();
        }    
        function showAddProcessFrame(){
          $.layer({
              type: 2,
              maxmin: false,
              shadeClose: false,
              title: '',
              shade: [0.0,'#fff'],
              offset: ['20px',''],
              area: ['800px', ($(window).height() - 150) +'px'],
              iframe: {src: '../addProcessIndex'},
              close: function(){
                // alert('222');
                table.ajax.reload();
                // location.reload();
              }
          });
        }
        function deleteRow(){
          var selectedIndex = table.row('.selected')[0];
          if(_.size(selectedIndex)  > 0){
              var obj = table.data()[selectedIndex[0]];
              $.post('../deleteProcessConfig', obj, function(data){
                // data = JSON.parse(data);
                if(data.code == 0){
                  layer.alert('已经成功删除！', 1);
                  table.ajax.reload();
                  return;
                }else if(data.code == 1){
                  layer.alert(data.message);
                  return;
                }
              })            
          }

        }
  

        function addDevice(_cmd){
            var txt = _cmd.btnName == null ? '':_cmd.btnName;
            var ele = 
              '<div class="col-xs-6 col-sm-6 col-md-2 col-md-lg-2 btnPanel" id = "btnPanel_'+ _cmd.flag +'">'+
                '<div class="row">'+
                  '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">'+
                      '<img src = "'+ _cmd.imgSrc +'" id = "img_'+ _cmd.flag +'">'+
                  '</div>'+
                  '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">'+
                      '<div class="panelText" id = "txt_'+ _cmd.flag +'">'+ txt +'</div>'+
                  '</div>'+
                '</div>'+
              '</div>'

            $('#row_container').append(ele);
        }

        function createWebSocket() {
            websocket = new WebSocket(wsUri);
            websocket.onopen = function (evt) { onOpen(evt) };
            websocket.onclose = function (evt) { onClose(evt) };
            websocket.onmessage = function (evt) { onMessage(evt) };
            websocket.onerror = function (evt) { onError(evt) };
        }
        function onMessage(evt) {
            var _cmd = JSON.parse(evt.data);
            console.log(_cmd);
            if(_cmd.Content.length <= 0) return
            if(_cmd.Type != 3) return
            _cmd = JSON.parse(_cmd.Content)
            var rowsCount = table.rows()[0].length;
            for(var i = 0; i< rowsCount; i++){
                var data = table.row().nodes().data()[i];
                // console.log(data.deviceID);
                if(data.deviceID == _cmd.BtnName){
                  $(table.cell(i,2).node()).addClass( 'highlight' );
                }
            }            
        }         
        function onOpen(evt) {
          console.log('websocket open');
          websocket.send('');
          hideTip()
        }
        function onClose(evt) {
          console.warn('websocket closed');
          showTip()
          websocket=null
        }
        function onError(evt) {
          console.error('websocket error');
          showTip()
          websocket=null
        }      
        function checkConnectionStatus(){
          if(websocket == null){
            createWebSocket()
          }
        }
        function showTip(){
          $('#connectionStatusBar').css('border-bottom-color','rgb(250,0,0)')
        }
        function hideTip(){
          $('#connectionStatusBar').css('border-bottom-color','white')
            // $('#lblTip').css('display', 'none');
            // $('#lblDeviceListText').css('margin-top', '40px');
        }
    </script>
  </head>
  <body style="">
  
    <div id = "divNav" style=""> 
        <div class="container">
            <div class="row"  id = "" style="">
                <div class="col-xs-1 col-sm-1 col-md-1 col-md-lg-1">

                    <img id="imglogo" src="/images/logo3.png">
                </div>
                <div class="col-xs-11 col-sm-11 col-md-11 col-md-lg-11">
                    <div style="">
                          <a class= "navFunc" style="right:6px;" onclick = "openAboutWindow()">关于 </a>
                     </div>
                </div>
            </div>
        </div>  
    </div>

<!--     <div class="demo-panel-title"> 
        <div class = "container">
            <div class="row" style="margin-top:20px;">
                <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12">
                    <div  id= "sayingContent" style=""> </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12">
                    <div  id="sayingAuthor" style=""></div>
                </div>
                
            </div>
        </div>
    </div> -->

    <!-- <div id="connectionStatusBar" style="border-bottom: solid 5px; border-bottom-color: white; position: absolute;width: 100%;"> </div> -->
    <div class="container">
        <div class="row" id = "subNavBar" style="">
            <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12">
                <div id="lblTableTitleText" style=""> 
                  <span class="glyphicon glyphicon-arrow-right" style="margin-right: 3px;"></span>
                  <span style="font-size: 16px;"> 流程配置 </span></div>
            </div>
         </div>
     </div>

    <div class="container" id= "mainContainer" style="">


      <div class="row"  id = "row_container">
        <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12">
          <table id="dtProcess" class="display" cellspacing="0" width="100%" >
              <thead>
                  <tr>
                      <th>排序</th>
                      <th>环节名称</th>
                      <th>关联的设备</th>
                      <th>备注</th>
                  </tr>
              </thead>
          </table>

        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12" style="margin-top: 150px;margin-bottom: 10px;text-align:center;">
            <button type="button" class="btnMayDo" onclick= "showAddProcessFrame()" style="">添加</button>
            <button type="button" class="btnMayDo" onclick = "deleteRow()" style="margin-left:2%;">删除</button>
        </div>


      </div>
<!--       <div style="text-align:center;text-align: center; border-top-style: solid; border-top-width: 1px; padding-top: 20px; border-top-color: rgba(128,128,128,0.3); padding-bottom: 20px;"> 
        <button type="button" class="btn btn-danger btn-lg" style="" onclick = "resetDeviceConfig()">重新配置所有设备
        </button>
      </div>
 -->    
    </div>
    <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12" id = "tipContainer" style="">
            <div> 
                <span class="icon-pushpin" style="margin-right: 3px;"></span>
                <span style=""> 根据需要配置生产流程 </span>
            </div>
          </div>
        </div>
    </div>  

<!--    <div class="container" style="margin-left: 0px; margin-right: 0px; max-width: 100%; background-color: rgb(240,240,240); max-width: 100%; min-height: 60px;position: absolute; width: 100%; bottom: 0px;">
      <div class="row"  id = "row_container">
            <div class="col-xs-3 col-sm-3 col-md-3 col-md-lg-3" style=""> </div>
          <div class="col-xs-6 col-sm-6 col-md-6 col-md-lg-6" style="text-align: center; font-size: 15px; margin-bottom: 0px; margin-top: 20px; color: rgb(180,180,180);">
              北京精益生产科技发展有限公司
          </div>
            <div class="col-xs-3 col-sm-3 col-md-3 col-md-lg-3" style=""> </div>
        
      </div>
    </div>  -->
    
    <script src="/bootstrap/js/bootstrap.min.js"></script>
    <script src="/layer/layer.min.js"></script>
    <script src="/dataTable/jquery.dataTables.js"></script>
<!-- Modal -->
    <script language="javascript" type="text/javascript">
        function openAboutWindow(){
          $('#modalAbout').modal();
        }
    </script>
<div class="modal fade" id="modalAbout" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #00695c; color: white;">
        <button type="button" class="close" data-dismiss="modal"><!-- <span aria-hidden="true">&times;</span> --><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">关于</h4>
      </div>
      <div class="modal-body">
        <div style="text-align: center; font-size: 19px; margin-top: 20px;">
           {{.aboutInfo}}
        </div>
      </div>
      <div class="modal-footer" style="text-align: center;">
        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>
<!-- Modal End-->
  </body>
</html>
