<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>首页</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" 
          type="image/png" 
          href="/images/logo_pure.png">
    <!-- Loading Bootstrap -->
    <!-- <link href="bootstrap/css/bootstrap.css" rel="stylesheet"> -->
    <link href="/stylesheets/style.css" rel="stylesheet" media="screen">    
    <link href="/bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">    
    <style type="text/css">
      body,html {
        margin: 0;
        padding: 0;
        height:100%;
        background: black;
      }
      #lblTip{
        margin-top: 30px; text-align: center; font-size: 30px; color: red; margin-bottom: 20px;
      }
      #dtProcess{
        text-align: center;
      }
      th{
        text-align: center;
      }
      .btn{
        width: 100px;
      }

    </style>
    <!-- // <script data-main="javascripts/main" src="/javascripts/require.js"></script> -->
    <script type="text/javascript" src = "/javascripts/jquery.js"></script>
    <script type="text/javascript" src = "/javascripts/underscore.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            updateSaying();
            // setInterval(updateSaying, 30 * 1000);
        });
        function updateSaying(){
            $.get('/GetRandomSaying', function(_data){
                console.log(_data);
                var saying = JSON.parse(_data);
                $('#sayingContent').text(saying.Content);
                $('#sayingAuthor').text(saying.Author);
            })
        }

    </script>
  </head>
  <body style="background-color:rgb(238,238,238);height:100%">
    <div style="min-height:100%; position: relative;">
        <div id = "divNav">
            <div class="container">
                <div class="row"  id = "">
                    <div class="col-xs-1 col-sm-1 col-md-1 col-md-lg-1">

                        <img id="imglogo" src="/images/logo3.png">
                    </div>
                    <div class="col-xs-11 col-sm-11 col-md-11 col-md-lg-11">
                        <div style=" ">
                              <a class= "navFunc" style="right:260px;" href="/GameSimulatorIndex">教程 </a>
                              <a class= "navFunc " style="right:170px;" href="/StartGameIndex" target="_blank">开始实验 </a>
                              <a class= "navFunc " style="right:80px;" href="/processConfigIndex" target="_blank">流程设置 </a>
                              <a class= "navFunc" style="right:20px;" onclick = "openAboutWindow()">关于 </a>
                         </div>
                    </div>
                </div>
            </div>
        </div>    
        <div class="main-panel">
          <!-- <img src="/images/mainBackground.png" style="width:100%;">  -->
            <!-- <h3  style="text-align:center;font-size:20px;color: white;margin-bottom: 10px;"></h3> -->
            <div style = "background-image:url(/images/mainBackground.png);height:704px;background-repeat:no-repeat;background-position-x:center;"> 
                <div class = "container">
                    <div class="row" style="margin-top:70px;">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12">
                            <div id= "sayingContent" style="text-align:left;font-size:28px;color: white;"> </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12">
                            <div id="sayingAuthor" style="text-align:right;font-size:20px;color: white;margin-right:10px;margin-top: 15px;"></div>
                        </div>
                        
                    </div>
                </div>
            </div>

        </div>
        <!-- <div id="lblTip" style="">设备发生变化，检查设备或者重新配置</div> -->
        <div class="container" style="background-color: #00695c; margin-left: 0px; margin-right: 0px; position: absolute; bottom: 0;max-width: 100%;width: 100%;">
          <div class="row"  id = "row_container">
              <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12" style="text-align: center; font-size: 15px; margin-bottom: 9px; margin-top: 14px; color: rgb(220,220,220);">
                  北京精益生产科技发展有限公司
              </div>
            
          </div>
        </div>
    </div>

<!-- Modal -->
    <script language="javascript" type="text/javascript">
        function openAboutWindow(){
          $('#modalAbout').modal();
        }
    </script>
<div class="modal fade" id="modalAbout" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #00695c; color: white;">
        <button type="button" class="close" data-dismiss="modal"><!-- <span aria-hidden="true">&times;</span> --><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">关于</h4>
      </div>
      <div class="modal-body">
        <div style="text-align: center; font-size: 19px; margin-top: 20px;">
           {{.aboutInfo}}
        </div>
      </div>
      <div class="modal-footer" style="text-align: center;">
        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>
<!-- Modal End-->
    
    <script src="/bootstrap/js/bootstrap.min.js"></script>
    <script src="/layer/layer.min.js"></script>
  </body>
</html>
