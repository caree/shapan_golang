<!DOCTYPE html>
<meta charset="utf-8" />
<title>游戏时间线</title>
<script language="javascript" type="text/javascript">
    // var wsUri = "ws://localhost:3003/";
	var wsUri = 'ws:' + window.location.href.substring(window.location.protocol.length)+ 'ws?guid={{.guid}}';

    var output;
    function init() {
        output = document.getElementById("output");
        createWebSocket();
        setInterval(checkConnectionStatus, 5*1000)

    }
    function createWebSocket() {
        websocket = new WebSocket(wsUri);
        websocket.onopen = function (evt) { onOpen(evt) };
        websocket.onclose = function (evt) { onClose(evt) };
        websocket.onmessage = function (evt) { onMessage(evt) };
        websocket.onerror = function (evt) { onError(evt) };
    }
    function onOpen(evt) {
        writeToScreen("已连接到服务器");
    }
    function onClose(evt) {
        writeToScreen("连接已断开");
        websocket=null

    }
    function onMessage(evt) {
        // writeToScreen('<span style="color: blue;">RESPONSE: ' + evt.data+'</span>');
        var cmd = JSON.parse(evt.data);
        if(cmd.Type==4){
            writeToScreen(cmd.Content)
        }
    }
    function onError(evt) {
        writeToScreen('<span style="color: red;">ERROR:</span> ' + evt.data);
        websocket=null
    }
    function doSend(message) {
        writeToScreen("SENT: " + message);
        websocket.send(message);
    }
    function writeToScreen(message) {
        var pre = document.createElement("p");
        pre.style.wordWrap = "break-word";
        pre.innerHTML = message;
        output.appendChild(pre);
    }
    function checkConnectionStatus(){
      if(websocket == null){
        createWebSocket()
      }
    }    
    window.addEventListener("load", init, false);
</script>

<h2>游戏时间线</h2>
<div id="output"></div>
</html>
  
  