package controllers

import (
	// "encoding/json"
	// "database/sql"
	"fmt"
	"github.com/astaxie/beego"
	"strconv"
	"time"
	// _ "github.com/mattn/go-sqlite3"
	// "io/ioutil"
	// "startbeego/controllers"
)

type OrderController struct {
	beego.Controller
}
type ProductionOrder struct {
	Index            int    `json:"index"`
	Name             string `json:"name"`
	Quantity         int    `json:"quantity"`
	Completed        bool   `json:"Completed"`
	Time             string `json:"time"`
	ProductTypeIndex int
}

func (this ProductionOrder) toProductionPlans(pieceValue, existedInventory int) (plans PlanList, inventory int) {
	// beego.Debug(fmt.Sprintf("ProductionOrder => toProductionPlans  pieceValue: %d   existedInventory: %d", pieceValue, existedInventory))
	// beego.Debug(this.toString())
	plans = PlanList{}
	inventory = 0
	if pieceValue <= 0 {
		return
	}
	if existedInventory > this.Quantity {
		inventory = existedInventory - this.Quantity
		return
	} else {
		this.Quantity = this.Quantity - existedInventory
	}

	for {
		if this.Quantity <= 0 {
			break
		}
		pro := ProductionPlan{
			Name:             this.Name,
			Quantity:         pieceValue,
			Completed:        false,
			ProductTypeIndex: this.ProductTypeIndex,
		}
		plans = append(plans, pro)
		if this.Quantity > pieceValue {
			this.Quantity = this.Quantity - pieceValue
		} else {
			inventory = pieceValue - this.Quantity
			break
		}
	}
	// beego.Debug(plans.printSelf())
	return
}

//输入产成品数量，如果订单被交付，返回true，否则false
//同时返回的还有产成品的剩余数量
func (this *ProductionOrder) updateCompletationStatus(count int) (bool, int) {
	if count >= this.Quantity {
		this.Completed = true
		return true, count - this.Quantity
	}
	return false, count
}
func (this ProductionOrder) toString() string {
	return fmt.Sprintf("订单信息 =>  Index: %d | Name: %s | Quantity: %d | Completed: %t | %s", this.Index, this.Name, this.Quantity, this.Completed, this.Time)
	// return fmt.Sprintf("%+v", this)
}

type OrderList4DataTable struct {
	Data []ProductionOrder `json:"data"`
}
type ProductType struct {
	Index int    `json:"index"`
	Name  string `json:"name"`
}
type command struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}
type OrderList []ProductionOrder

func (this *OrderList) addNewOrder(productType ProductType, quantity int) ProductionOrder {
	index := len(*this) + 1
	t := time.Now() //获取当前时间的结构体
	po := ProductionOrder{
		Index:            index,
		Name:             productType.Name,
		Quantity:         quantity,
		Completed:        false,
		Time:             t.Format("2006-01-02 15:04:05"),
		ProductTypeIndex: productType.Index,
	}
	(*this) = append(*this, po)
	return po
}
func (this *OrderList) checkInventory(productionInfoList ProductionInfoList) ProductionInfoList {
	for i := 0; i < len(*this); i++ {
		order := (*this)[i]
		for j := 0; j < len(productionInfoList); j++ {
			if productionInfoList[j].ProductTypeIndex == order.ProductTypeIndex {
				productionInfoList[j].OffLineProductCount -= order.Quantity
			}
		}
	}
	return productionInfoList
}

// func (this *OrderList) sumOrderProduct(productType, producedCount int) bool {
// 	total := 0
// 	leftCount:=producedCount
// 	bCompleted=false
// 	for i := 0; i < len(this); i++ {
// 		if this[i].ProductTypeIndex == productType {
// 			total += this[i].Quantity
// 			if bCompleted,leftCount= this[i].updateCompletationStatus(leftCount);bCompleted==false{

// 			}
// 		}
// 	}
// 	return false
// }

//传入产成品数量，确定是否有订单可以交付
//如果订单全部交付，返回true，否则false，
func (this *OrderList) updateCompletationStatus(productionInfoList ProductionInfoList) bool {
	// beego.Alert("updateCompletationStatus =>")
	// beego.Alert(productionInfoList.printSelf())
	completed := true
	bCompleted := false
	for i := 0; i < len(productionInfoList); i++ { //轮询各个类型的产品
		info := productionInfoList[i]
		leftCount := info.OffLineProductCount
		for j := 0; j < len(*this); j++ { //轮询订单，类型相同就完成
			if (*this)[j].ProductTypeIndex == info.ProductTypeIndex {
				if bCompleted, leftCount = (*this)[i].updateCompletationStatus(leftCount); bCompleted == false {
					completed = false
				}
			}
		}
	}

	// orderedCount := this.sumOrderProduct(info.ProductTypeIndex)
	// if orderedCount > 0 && orderedCount > info.OffLineProductCount {
	// 	completed = false
	// 	break
	// }
	// for i := 0; i < len(*this); i++ {
	// 	bCovered, leftCount := (*this)[i].updateCompletationStatus(count)
	// 	if !bCovered {
	// 		return false, leftCount
	// 	} else {
	// 		beego.Debug("完成订单：" + (*this)[i].toString())
	// 		return this.updateCompletationStatus(leftCount)
	// 	}
	// }
	return completed
}

func (this OrderList) getCoverRate() int {
	if len(this) <= 0 {
		return 0
	}
	length := len(this)
	coveredLenth := 0
	for i := 0; i < length; i++ {
		if this[i].Completed == true {
			coveredLenth++
		}
	}
	return int(100 * coveredLenth / length)
}

func (this OrderList) printSelf() string {
	str := "\r\n"
	for i := 0; i < len(this); i++ {
		str += this[i].toString() + "\r\n"
		// beego.Emergency(this[i].toString())
	}
	return str
}

type InventoryInfo struct {
	productTypeIndex int
	inventory        int
}

func (this *InventoryInfo) setInventory(inventory int) {
	this.inventory = inventory
}

type InventoryInfoList []InventoryInfo

func (this *InventoryInfoList) setInventory(productType, inventory int) {
	for i, typeInfo := range *this {
		if typeInfo.productTypeIndex == productType {
			(&(*this)[i]).setInventory(inventory)
			break
		}
	}
}
func (this *InventoryInfoList) getInventory(productType int) int {
	for _, typeInfo := range *this {
		if typeInfo.productTypeIndex == productType {
			return typeInfo.inventory
		}
	}
	return 0
}
func (this OrderList) toProductionPlans(pieceValue int) PlanList {
	// beego.Debug(this.printSelf())
	// beego.Debug(fmt.Sprintf("toProductionPlans => %d", pieceValue))
	plans := PlanList{}
	// var plansTemp PlanList
	// inventory := 0
	inventoryInfoList := InventoryInfoList{}
	for _, typeInfo := range g_productTypeList {
		inventoryInfoList = append(inventoryInfoList, InventoryInfo{typeInfo.Index, 0})
	}
	for i := 0; i < len(this); i++ {
		order := this[i]
		plansTemp, inventoryTemp := order.toProductionPlans(pieceValue, inventoryInfoList.getInventory(order.ProductTypeIndex))
		// plansTemp, inventoryTemp := order.toProductionPlans(pieceValue, inventory)
		plans = append(plans, plansTemp...)
		// inventory = inventoryTemp
		inventoryInfoList.setInventory(order.ProductTypeIndex, inventoryTemp)
	}
	for i := 0; i < len(plans); i++ {
		plans[i].setIndex(i + 1)
	}
	// beego.Debug(plans.printSelf())
	return plans
}

func init() {

	/*
		rowList := RowMapList{
			RowMap{2, 11},
			RowMap{3, 9},
			RowMap{4, 10},
			RowMap{5, 12},
			RowMap{6, 13},
			RowMap{7, 14},
			RowMap{9, 16},
			RowMap{10, 19},
			RowMap{11, 20},
			RowMap{12, 21},
			RowMap{13, 22},
			RowMap{14, 23},
			RowMap{15, 24},
			RowMap{17, 26},
			RowMap{18, 27},
			RowMap{19, 28},
			RowMap{20, 29},
			RowMap{21, 30},
			RowMap{22, 31},
			RowMap{23, 32},
			RowMap{24, 33},
			RowMap{25, 34},
			RowMap{26, 35},
			RowMap{27, 36},
			RowMap{28, 37},
			RowMap{29, 38},
			RowMap{30, 39},
			RowMap{31, 40},
			RowMap{32, 41},
			RowMap{33, 42},
			RowMap{34, 43},
			RowMap{36, 45},
			RowMap{37, 46},
			RowMap{38, 47},
			RowMap{39, 48},
			RowMap{40, 49},
			// RowMap{41, 50},
			RowMap{42, 51},
			RowMap{43, 52},
			RowMap{45, 54},
			// RowMap{46, 55},
			RowMap{47, 56},
			RowMap{48, 57},
			RowMap{49, 58},
			RowMap{55, 64},
			RowMap{56, 65},
			RowMap{57, 66},
			RowMap{58, 67},
			RowMap{59, 68},
			RowMap{60, 69},
			RowMap{61, 70},
			RowMap{62, 71},
			RowMap{64, 73},
			RowMap{65, 74},
			RowMap{66, 75},
			RowMap{67, 76},
			RowMap{68, 77},
			RowMap{69, 78},
			RowMap{71, 80},
			RowMap{72, 81},
		}
		rowList.printSelf()

		columnList := ColumnMapList{
			ColumnMap{1, 4},
			ColumnMap{2, 5},   //
			ColumnMap{5, 14},  //
			ColumnMap{6, 15},  //
			ColumnMap{9, 6},   //
			ColumnMap{10, 7},  //
			ColumnMap{13, 9},  //
			ColumnMap{14, 10}, //
		}
		columnList.printSelf()
		beego.Debug("*************************************************")
		cellMapList := outputCellMap(rowList, columnList)
		str := cellMapList.printSelf()
		beego.Debug("*************************************************")
		ioutil.WriteFile("map.txt", []byte(str), 0644)
	*/
	// from db
	/*
		if rows, err := db.Query("SELECT * FROM productOrder"); err == nil {
				for rows.Next() {
					var Name, AddTime string
					var OrderIndex, Quantity, ProductTypeIndex int
					var Completed bool
					if rows.Scan(&OrderIndex, &Name, &Quantity, &Completed, &AddTime, &ProductTypeIndex) == nil {
						orderList = append(orderList, ProductionOrder{OrderIndex, Name, Quantity, Completed, AddTime, ProductTypeIndex})
					}
				}
			}
			beego.Trace("order init...")
			// beego.Trace(orderList)
			orderList.printSelf()
	*/
}

// func (this *OrderController) OrderConfigIndex() {
// 	this.TplNames = "orderConfigIndex.tpl"
// }
// func (this *OrderController) AddOrderIndex() {
// 	this.TplNames = "addOrderIndex.tpl"
// }
// func (this *OrderController) OrderList() {
// 	// beego.Trace(orderList)
// 	this.Data["json"] = OrderList4DataTable{Data: g_orderList}
// 	this.ServeJson()
// }
func (this *OrderController) ProductTypeList() {
	this.Data["json"] = g_productTypeList
	this.ServeJson()
}

type RowMap struct {
	s int
	d int
}

func (this RowMap) toString() string {
	return "row: " + strconv.Itoa(this.s) + " => " + strconv.Itoa(this.d+8)
}

type RowMapList []RowMap

func (this *RowMapList) add(s, d int) {
	// rowMap :=
	*this = append(*this, RowMap{s, d})
}
func (this RowMapList) printSelf() {
	for i := 0; i < len(this); i++ {
		beego.Emergency(this[i].toString())
	}
}

type ColumnMap struct {
	s int
	d int
}

func (this ColumnMap) toString() string {
	return "column: " + strconv.Itoa(this.s) + " => " + strconv.Itoa(this.d+4)
}

type ColumnMapList []ColumnMap

func (this *ColumnMapList) add(s, d int) {
	*this = append(*this, ColumnMap{s, d})
}
func (this ColumnMapList) printSelf() {
	for i := 0; i < len(this); i++ {
		beego.Emergency(this[i].toString())
	}
}

type CellMap struct {
	RowSrc    int
	ColumnSrc int
	RowDst    int
	ColumnDst int
}

func (this CellMap) toString() string {
	// return this.Row.toString() + " | " + this.Column.toString()
	return strconv.Itoa(this.RowSrc) + "." + strconv.Itoa(this.ColumnSrc) + "=" + strconv.Itoa(this.RowDst) + "." + strconv.Itoa(this.ColumnDst)
}

type CellMapList []CellMap

func (this CellMapList) printSelf() string {
	str := ""
	for i := 0; i < len(this); i++ {
		str += this[i].toString() + "\n"
		fmt.Println(this[i].toString())
	}
	return str
}
func outputCellMap(rowMapList RowMapList, columnMapList ColumnMapList) CellMapList {
	cellMapList := CellMapList{}
	rowListLength := len(rowMapList)
	columnListLength := len(columnMapList)

	for i := 0; i < rowListLength; i++ {
		for j := 0; j < columnListLength; j++ {
			rowMap := rowMapList[i]
			columnMap := columnMapList[j]
			cellMapList = append(cellMapList, CellMap{
				RowSrc:    rowMap.s,
				RowDst:    rowMap.d,
				ColumnSrc: columnMap.s,
				ColumnDst: columnMap.d,
			})
		}
	}
	return cellMapList
}

// func (this *OrderController) AddOrder() {
// 	beego.Trace("AddOrder =>")
// 	beego.Trace(string(this.Ctx.Input.RequestBody))
// 	productTypeIndex := this.GetString("productTypeIndex")
// 	quantity := this.GetString("quantity")
// 	intProductTypeIndex, err1 := strconv.Atoi(productTypeIndex)
// 	intQuantity, err2 := strconv.Atoi(quantity)
// 	if err2 != nil || err1 != nil {
// 		this.Data["json"] = command{Code: 1, Message: "添加订单出现异常！"}

// 	} else {
// 		if b, msg := addNewOrder(intProductTypeIndex, intQuantity); b {
// 			this.Data["json"] = command{Code: 0, Message: ""}
// 		} else {
// 			this.Data["json"] = command{Code: 1, Message: msg}
// 		}
// 	}
// 	this.ServeJson()
// }

// func (this *OrderController) DeleteOrder() {
// 	//delete form db 	then from temp
// 	beego.Trace("DeleteOrder =>")
// 	beego.Trace(string(this.Ctx.Input.RequestBody))
// 	orderIndex := this.GetString("index")
// 	if intOrderIndex, err := strconv.Atoi(orderIndex); err == nil {
// 		if stmt, err1 := db.Prepare("delete from productOrder where OrderIndex=?"); err1 == nil {
// 			if _, err2 := stmt.Exec(intOrderIndex); err2 == nil {
// 				g_orderList.removeOrderByIndex(intOrderIndex)
// 				this.Data["json"] = command{Code: 0, Message: ""}
// 				goto ServeJson
// 			}
// 		}
// 	}
// 	this.Data["json"] = command{Code: 1, Message: "删除订单出现异常"}
// ServeJson:
// 	this.ServeJson()
// }

// func addNewOrder(index int, quantity int) (bool, string) {
// 	count := len(g_productTypeList)
// 	for i := 0; i < count; i++ {
// 		if g_productTypeList[i].Index == index {
// 			productInfo := g_productTypeList[i]
// 			t := time.Now() //获取当前时间的结构体
// 			newOrder := ProductionOrder{Index: 1, Name: productInfo.Name, Quantity: quantity, Completed: false, Time: t.Format("2006-01-02 15:04:05"), ProductTypeIndex: productInfo.Index}
// 			if b, orderTemp := g_orderList.addNewOrder(newOrder); b {
// 				//db
// 				if stmt, err := db.Prepare("INSERT INTO productOrder(OrderIndex, Name, Quantity, Completed, AddTime, ProductTypeIndex) values(?,?,?,?,?,?)"); err == nil {
// 					if _, err = stmt.Exec(orderTemp.Index, orderTemp.Name, orderTemp.Quantity, orderTemp.Completed, orderTemp.Time, orderTemp.ProductTypeIndex); err == nil {
// 						beego.Trace("New Order Added")
// 						beego.Trace(orderTemp)
// 						return true, ""
// 					} else {
// 						g_orderList.removeOrder(orderTemp)
// 						beego.Error(err)
// 					}
// 				} else {
// 					g_orderList.removeOrder(orderTemp)
// 					beego.Error(err)
// 				}

// 			}
// 		}
// 	}
// 	return false, "添加订单出现异常！"
// }
// func (this OrderList) addNewOrder(_order ProductionOrder) (bool, ProductionOrder) {
// 	maxIndex := 1
// 	count := len(g_orderList)
// 	for i := 0; i < count; i++ {
// 		if g_orderList[i].Index > maxIndex {
// 			maxIndex = g_orderList[i].Index
// 		}
// 	}
// 	_order.Index = maxIndex + 1
// 	g_orderList = append(g_orderList, _order)
// 	return true, _order
// }
// func (this OrderList) removeOrder(_order ProductionOrder) bool {
// 	var orderListTemp = OrderList{}
// 	count := len(g_orderList)
// 	for i := 0; i < count; i++ {
// 		if g_orderList[i].Index != _order.Index {
// 			orderListTemp = append(orderListTemp, g_orderList[i])
// 		}
// 	}
// 	g_orderList = orderListTemp
// 	return true
// }
// func (this OrderList) removeOrderByIndex(_orderIndex int) bool {
// 	var orderListTemp = OrderList{}
// 	count := len(g_orderList)
// 	for i := 0; i < count; i++ {
// 		if g_orderList[i].Index != _orderIndex {
// 			orderListTemp = append(orderListTemp, g_orderList[i])
// 		}
// 	}
// 	g_orderList = orderListTemp
// 	return true
// }
