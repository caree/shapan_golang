package controllers

import (
	// "encoding/json"
	"fmt"
	"github.com/astaxie/beego"
	// "startbeego/models"
	"strconv"
	"time"
	// "math/rand"
	"errors"
)

type processLoopState int

const (
	notYetStarted = 0
	goingOn       = 1
	over          = 2
)

func (this processLoopState) toString() string {
	str := ""
	switch this {
	case notYetStarted:
		str = "未开始"
	case goingOn:
		str = "进行中"
	case over:
		str = "已结束"
	}
	return str
}

type ProcessLoop struct {
	ProcessStateList
	state            processLoopState
	startStamp       int64
	endStamp         int64
	startTime        string
	endTime          string
	loopIndex        int
	nPieceValue      int
	ProductTypeIndex int
	qcState          bool
}

func (this ProcessLoop) countProcessTimeElapse() (error, ProcessBalanceResult) {
	length := len(this.ProcessStateList)
	for i := length - 1; i >= 0; i-- {
		// fmt.Println(i)
		stateTemp := (this.ProcessStateList)[i]
		// fmt.Println(stateTemp.toString())
		// beego.Trace("countProcessTimeElapse => " + stateTemp.toString())
		if stateTemp.completed() {
			result := ProcessBalanceResult{
				LoopIndex:  this.loopIndex,
				LoopName:   "生产批次" + strconv.Itoa(this.loopIndex),
				Index:      stateTemp.Index,
				Name:       stateTemp.Name,
				TimeElapse: int(stateTemp.endStamp - stateTemp.startStamp),
			}
			return nil, result
		} else {
			// beego.Error("countProcessTimeElapse => completed false  i= " + strconv.Itoa(i))
			// fmt.Println("error i = " + strconv.Itoa(i))
		}
	}
	return errors.New("产生生产流程消耗时间数据失败"), ProcessBalanceResult{}
}
func (this *ProcessLoop) setState(state processLoopState) {
	this.state = state
}
func (this ProcessLoop) toString() string {
	// return fmt.Sprintf("%+v", this)
	return fmt.Sprintf("批次信息 =>  loopIndex: %d | startTime: %s | endTime: %s | nPieceValue: %d | state: %s | qcState: %t",
		this.loopIndex, this.startTime, this.endTime, this.nPieceValue, this.state.toString(), this.qcState)
}
func (this ProcessLoop) currentState() processLoopState {
	return this.state
}

func (this *ProcessLoop) setNpieceValue(value int) {
	this.nPieceValue = value
}
func (this *ProcessLoop) setQCState(qc bool) {
	this.qcState = qc
}
func (this *ProcessLoop) accepted(btnCommand BtnCommand) (accepted, loopEnded bool) {
	// if this.currentState() == goingOn {
	for i := 0; i < len(this.ProcessStateList); i++ {
		processState := &this.ProcessStateList[i]
		if processState.DeviceID == btnCommand.BtnName {
			if btnCommand.open() && !processState.started() {
				startResult := false
				if i == 0 { //这是第一个流程，直接开始
					beego.Alert("这是第一个流程，直接开始")
					startResult = processState.start(btnCommand)
					beego.Alert("流程信息已更新")
					// beego.Debug(processState.toString())
					// this.ProcessStateList.printSelf()
				} else {
					beego.Alert("这不是是第一个流程，首先检查上一个流程")

					//如果不是除总流程外的第一流程，需要保证其上一个流程已经完成
					upperProcess := (this.ProcessStateList)[i-1]
					if upperProcess.completed() {
						beego.Debug(upperProcess.Name + " 已经完成，可以开始下一个流程")
						startResult = processState.start(btnCommand)
					} else {
						beego.Alert("上一个流程尚未完成，该流程不能开始")
						newTimeLineMessage("上一个流程尚未完成，该流程不能开始").broadcastTimeLineMessage()
						return false, false
					}
				}
				if startResult {
					this.state = goingOn
					t := time.Now() //获取当前时间的结构体
					this.startTime = t.Format("2006-01-02 15:04:05")
					this.startStamp = t.Unix()
				}
				return startResult, false
			}

			if !processState.completed() {

				if btnCommand.closeNormal() && !processState.isQCprocess() {
					stopResult := processState.stop(btnCommand)
					// fmt.Println(processState.toString())
					// beego.Debug("close state: " + processState.toString())
					return stopResult, false
				}
				if processState.isQCprocess() {
					stopResult := processState.stop(btnCommand)
					this.state = over
					t := time.Now() //获取当前时间的结构体
					this.endTime = t.Format("2006-01-02 15:04:05")
					this.endStamp = t.Unix()

					if btnCommand.closeNormal() {
						this.setQCState(true)

						str1 := "质检流程结束，产品质量合格"
						beego.Alert(str1)
						newTimeLineMessage(str1).broadcastTimeLineMessage()
					} else if btnCommand.closeYAS() {
						// b := processState.stop(btnCommand)
						this.setQCState(false)
						// this.state = over
						str1 := "质检流程结束，产品质量不合格"
						beego.Alert(str1)
						newTimeLineMessage(str1).broadcastTimeLineMessage()
						// return b, true
					}
					return stopResult, true
				}

			}
		}
	}
	// }
	return false, false
}

//****************************************************************************************
// ProcessLoopList

type ProcessLoopList []ProcessLoop

func (this *ProcessLoopList) updateProcessLoop(btnCommand BtnCommand) (updated, loopEnded bool, loop ProcessLoop) {
	length := len(*this)
	for i := 0; i < length; i++ {
		loop := &((*this)[i])
		accepted, loopEnded := loop.accepted(btnCommand)
		if accepted {
			if loopEnded { //流程结束
				return true, true, (*this)[i]
			}
			return true, false, (*this)[i]
		}
	}
	return false, false, ProcessLoop{}
}

// func (this *ProcessLoopList) countProcessTimeElapse(loop ProcessLoop) ProcessBalanceResult {

// 	// processBalanceResults.addResult(state.loopIndex, state.Index, state.Name, int(state.startStamp), int(state.endStamp))
// 	// processBalanceResults.printSelf()
// }
func (this *ProcessLoopList) countProductCount() ProductionInfoList {
	// (onLineProductCount, offLineProductCountQcPassed, offLineProductCountQcNotPassed int)
	pinfoList := ProductionInfoList{}
	for i := 0; i < len(g_productTypeList); i++ {
		pinfoList.addNullInfo(g_productTypeList[i])
	}
	// offLineProductCountQcPassed, offLineProductCountQcNotPassed, onLineProductCount = 0, 0, 0
	length := len(*this)
	for i := 0; i < length; i++ {
		loop := (*this)[i]
		if loop.currentState() == over && loop.qcState == true {
			pinfoList.addProductCount(loop.ProductTypeIndex, -1, loop.nPieceValue, -1)
			// beego.Debug(pinfoList.printSelf())

			// offLineProductCountQcPassed += loop.nPieceValue
		}
		if loop.currentState() == over && loop.qcState == false {
			pinfoList.addProductCount(loop.ProductTypeIndex, -1, -1, loop.nPieceValue)
			// offLineProductCountQcNotPassed += loop.nPieceValue
		}
		if loop.currentState() == goingOn {
			pinfoList.addProductCount(loop.ProductTypeIndex, loop.nPieceValue, -1, -1)
			// onLineProductCount += loop.nPieceValues
		}
	}
	return pinfoList
}
func (this ProcessLoopList) hasNullLoop() bool {
	// beego.Warn("hasNullLoop => ")
	length := len(this)
	// fmt.Println("hasNullLoop =>")
	// fmt.Println(length)
	if length > 0 {
		if (this)[length-1].currentState() == notYetStarted {
			// beego.Warn(" => true")
			return true
		}
	}
	// this.printSelf()
	return false
}
func (this *ProcessLoopList) setAllOnlineProductsUnquanlified() (count int) {
	count = 0
	length := len(*this)
	for i := 0; i < length; i++ {
		if looTemp := &(*this)[i]; looTemp.currentState() == goingOn {
			looTemp.setState(over)
			looTemp.setQCState(false)
			count++
		}
	}
	return
}

func (this *ProcessLoopList) addNewProcessLoop(pieceValue, productType int) {
	// fmt.Println("addNewProcessLoop =>")
	length := len(*this)
	loopIndex := 0
	if length > 0 {
		loopIndex = (*this)[length-1].loopIndex + 1
	}
	// fmt.Println("...1111")
	processStateListTemp := ProcessStateList{}
	for i := 1; i < len(processList); i++ {
		processStateListTemp = append(processStateListTemp, ProductionProcessState{
			ProductionProcess: processList[i],
			state:             "off",
			startStamp:        0,
			endStamp:          0,
			startTime:         "",
			endTime:           "",
			loopIndex:         loopIndex})
	}
	// fmt.Println("...2222")
	processLoop := ProcessLoop{
		ProcessStateList: processStateListTemp,
		state:            notYetStarted,
		startStamp:       0,
		endStamp:         0,
		startTime:        "",
		endTime:          "",
		loopIndex:        loopIndex,
		nPieceValue:      pieceValue,
		qcState:          true,
		ProductTypeIndex: productType,
	}
	(*this) = append(*this, processLoop)
}
func (this ProcessLoopList) printSelf() string {
	str := ""
	for i := 0; i < len(this); i++ {
		str += this[i].toString() + "\r\n"
		// beego.Trace(this[i].toString())
	}
	return str
}
