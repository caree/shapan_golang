<!DOCTYPE html>
<meta charset="utf-8" />
<title>生产效率实验</title>
    <link href="/bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">    
    <link rel='stylesheet' href='/stylesheets/c3.css' />
    <script type="text/javascript" src = "/javascripts/jquery.js"></script>
    <script type="text/javascript" src = "/javascripts/d3.js"></script>
    <script type="text/javascript" src = "/javascripts/c3.js"></script>
    <script type="text/javascript" src = "/javascripts/underscore.js"></script>

<script language="javascript" type="text/javascript">
    // var wsUri = "ws://localhost:3003/";
	var wsUri = 'ws:' + window.location.href.substring(window.location.protocol.length)+ 'ws?guid={{.guid}}';
    var chart;
    var threePieceFlow = '三件流'
    var singlePieceFlow = '单件流'
    var nPieceFlowMessages = []
    var chartStyle = "null"
    var output;
    function init() {
        output = document.getElementById("output");
        submitSelect(function(){
            setToOnlineChart()
            createWebSocket();
            setInterval(checkConnectionStatus, 5*1000)               
        })
                   
    }
    function setToOnlineChart(){
        if(chartStyle == 'online') return
        chartStyle = 'online'      
        buildChart()
    }
    function setToOfflineChart(){
        if(chartStyle == 'offline') return
        chartStyle = 'offline'
        buildChart()
    }
    function submitSelect(callback){
        var radioes = $('[name="optionsRadios"]')
        for (var i = 0; i < radioes.length; i++) {
            var radio = radioes[i]
            // console.log(radio)
            if(radio.checked == true){
                var value = parseInt(radio.value)
                $.post('../SetNpiece', {Nvalue: value}, function(data){
                    if(data.code == 0){
                      console.log('设置成功')
                      if(callback != null){
                        callback()
                      }
                        // createWebSocket();
                        // setInterval(checkConnectionStatus, 5*1000)                      
                    }else if(data.code == 1){
                      layer.alert(data.message);
                    }
                })                  
                break
            }
        };
    }
    function buildChart(){
        chart = c3.generate({
            data: {
                    xs: {
                        '三件流': 'x1',
                        '单件流': 'x2',
                    },
                    columns: buildChartData()
                    // [
                    //     ['x1', 0],
                    //     ['x2', 0],
                    //     ['三件流', 0],
                    //     ['单件流', 0]
                    // ]
            }
        });          
    }
    function buildChartData(){
        if(_.size(nPieceFlowMessages) <= 0){
            var rtn = [['x1', 0], ['x2', 0], ['三件流', 0], ['单件流', 0]]
            console.log(JSON.stringify(rtn))
            return rtn
        }
        var x1 = ['x1']
        var three = ['三件流']
        
        var x2 = ['x2']
        var single = ['单件流']

        _.each(nPieceFlowMessages, function(_message){
            if(_message.NpieceValue == 3){
                x1.push(_message.Timestamp)
                if(chartStyle == 'online'){
                    three.push(_message.OnLineProductCount)
                }else{
                    three.push(_message.OffLineProductCount)
                }
            }else if(_message.NpieceValue == 1){
                x2.push(_message.Timestamp)
                if(chartStyle == 'online'){
                    single.push(_message.OnLineProductCount)
                }else{
                    single.push(_message.offLineProductCount)
                }
            }
        }) 
        var rtn = [x1, x2, three, single]
        console.log(JSON.stringify(rtn))
        return  rtn
    }
    function updateNPieceFlowMessage(message){
        nPieceFlowMessages.push(message)
        // {"NpieceValue":3,"Time":"09:42:36","Timestamp":19,"OnLineProductCount":3,"OffLineProductCount":0,"OffLineProductCountQcNotPassed":0}

        chart.load({
            columns: buildChartData(),
        });

    }
    function createWebSocket() {
        websocket = new WebSocket(wsUri);
        websocket.onopen = function (evt) { onOpen(evt) };
        websocket.onclose = function (evt) { onClose(evt) };
        websocket.onmessage = function (evt) { onMessage(evt) };
        websocket.onerror = function (evt) { onError(evt) };
    }
    function onOpen(evt) {
        writeToScreen("已连接到服务器");
    }
    function onClose(evt) {
        writeToScreen("连接已断开");
        websocket=null
    }
    function onMessage(evt) {
        // writeToScreen('<span style="color: blue;">RESPONSE: ' + evt.data+'</span>');
        var cmd = JSON.parse(evt.data);
        switch(cmd.Type){
            case 4:
                writeToScreen(cmd.Content)
                break
            case 6:
                console.log(cmd.Content)
                updateNPieceFlowMessage(JSON.parse(cmd.Content))
                break
        }
    }
    function onError(evt) {
        writeToScreen('<span style="color: red;">ERROR:</span> ' + evt.data);
        websocket=null
    }
    function doSend(message) {
        writeToScreen("SENT: " + message);
        websocket.send(message);
    }
    function writeToScreen(message) {
        var pre = document.createElement("p");
        pre.style.wordWrap = "break-word";
        pre.innerHTML = message;
        output.appendChild(pre);
    }
    function checkConnectionStatus(){
      if(websocket == null){
        createWebSocket()
      }
    }    
    window.addEventListener("load", init, false);
</script>

<!-- <h2>N件流</h2> -->
<body> 
    <div style="text-align: center; font-size: 20px; margin-bottom: 10px;">选择生产批量</div>
    <div class="container" >

          <div class="row"  id = "row_container">
            <div class="col-xs-3 col-sm-3 col-md-3 col-md-lg-3"> </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-md-lg-6">

                <div class="radio">
                  <label>
                    <input type="radio" name="optionsRadios" id="optionsRadios1" value="3" checked>
                    三件流
                  </label>
                </div>
                <div class="radio">
                  <label>
                    <input type="radio" name="optionsRadios" id="optionsRadios2" value="1">
                    单件流
                  </label>
                </div>

            </div>
            <div class="col-xs-3 col-sm-3 col-md-3 col-md-lg-3">
            </div>
          </div>

          <div class="row"  id = "row_container" style="
    margin-top: 50px;">
            <div class="col-xs-3 col-sm-3 col-md-3 col-md-lg-3"> </div>
            <div class="col-xs-6 col-sm-6 col-md-6 col-md-lg-6">
                <button type="button" class="btn btn-primary btn-lg btn-block" onclick="submitSelect()">确定</button>
            </div>
            <div class="col-xs-3 col-sm-3 col-md-3 col-md-lg-3">
            </div>
          </div>

        <div class="row"  id = "row_container">
            <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12" style="margin-top:20px;">
                    <div id="lblTableTitleText" style="font-size:23px;padding-bottom: 15px;"> 生产效率实验 </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12" style="margin-top:5px;">

                <div id="chart" style=""></div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12" style="margin-top: 50px;text-align:center;">
                <button type="button" class="btn btn-default btn-sm" style="width: 150px;" onclick = "setToOnlineChart()">在线半成品统计图</button>
                <button type="button" class="btn btn-default btn-sm" style="width: 150px;" onclick = "setToOfflineChart()">产成品统计图</button>
            </div>
        </div>


    </div>
    <script src="/bootstrap/js/bootstrap.min.js"></script>
</body>
<div id="output"></div>
</html>
  
  