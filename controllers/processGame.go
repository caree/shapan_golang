package controllers

import (
	// "encoding/json"
	// "fmt"
	"github.com/astaxie/beego"
	// "startbeego/models"
	"strconv"
	"time"
	// "math/rand"
)

type GameController struct {
	beego.Controller
}

// **********************************************************************
// 生产效率实验
// *********************************
//订单页面
func (this *GameController) OrderList4ProductionEfficientIndex() {
	this.Data["aboutInfo"] = aboutInfo
	setRunningState(未设定)
	this.TplNames = "OrderList4ProductionEfficientIndex.tpl"
}

//订单数据
func (this *GameController) OrderList4ProductionEfficient() {
	value := this.GetString("value")
	list := OrderList{}
	if iValue, err := strconv.Atoi(value); err == nil {
		list = g_orderListMap[gameState(iValue)]
	}
	this.Data["json"] = OrderList4DataTable{Data: list}
	this.ServeJson()
}

//计划页面
func (this *GameController) PlanList4ProductionEfficientNPieceIndex() {
	this.Data["aboutInfo"] = aboutInfo
	setRunningState(未设定)
	this.TplNames = "PlanList4ProductionEfficientNPieceIndex.tpl"
}
func (this *GameController) PlanList4ProductionEfficientSinglePieceIndex() {
	setRunningState(未设定)
	this.Data["aboutInfo"] = aboutInfo
	this.TplNames = "PlanList4ProductionEfficientSinglePieceIndex.tpl"
}

//计划数据
func (this *GameController) PlanList4ProductionEfficient() {
	value := this.GetString("value")
	list := PlanList{}
	if iValue, err := strconv.Atoi(value); err == nil {
		list = g_PlanListMap[gameState(iValue)]
	}
	this.Data["json"] = PlanList4DataTable{Data: list}
	this.ServeJson()
}

//游戏实时页面
func (this *GameController) ProductionEfficientNpieceFlowIndex() {
	setRunningState(生产效率实验多件流)
	// resetGameEnvironment()
	this.Data["guid"] = GetGUID()
	this.Data["aboutInfo"] = aboutInfo
	this.TplNames = "ProductionEfficientNpieceFlowIndex.tpl"
}
func (this *GameController) ProductionEfficientSinglePieceFlowIndex() {
	setRunningState(生产效率实验单件流)
	// resetGameEnvironment()
	this.Data["guid"] = GetGUID()
	this.Data["aboutInfo"] = aboutInfo
	this.TplNames = "ProductionEfficientSinglePieceFlowIndex.tpl"
}

// **********************************************************************
// 生产平衡实验
// *********************************
func (this *GameController) PlanList4BalanceIndex() {
	setRunningState(未设定)
	this.Data["aboutInfo"] = aboutInfo
	this.TplNames = "PlanList4BalanceIndex.tpl"
}
func (this *GameController) BalanceProductionIndex() {
	setRunningState(生产平衡实验)
	// resetGameEnvironment()
	this.Data["pieceValue"] = g_productionBalance_PieceValue
	this.Data["guid"] = GetGUID()
	this.Data["aboutInfo"] = aboutInfo
	this.TplNames = "BalanceProductionIndex.tpl"
}

// **********************************************************************

// **********************************************************************
// 良品率实验
// *********************************
func (this *GameController) PlanList4QuanlifiedProductionNPieceIndex() {
	setRunningState(未设定)
	this.Data["aboutInfo"] = aboutInfo
	this.TplNames = "PlanList4QuanlifiedProductionNPieceIndex.tpl"
}
func (this *GameController) PlanList4QuanlifiedProductionSinglePieceIndex() {
	setRunningState(未设定)
	this.Data["aboutInfo"] = aboutInfo
	this.TplNames = "PlanList4QuanlifiedProductionSinglePieceIndex.tpl"
}
func (this *GameController) QuanlifiedRateNPieceIndex() {
	setRunningState(良品率多件流实验)
	// resetGameEnvironment()
	this.Data["pieceValue"] = g_planList4QuanlifiedProductionNPiece_PieceValue
	this.Data["guid"] = GetGUID()
	this.Data["aboutInfo"] = aboutInfo
	this.TplNames = "QuanlifiedRateNPieceIndex.tpl"
}
func (this *GameController) QuanlifiedRateSinglePieceIndex() {
	setRunningState(良品率单件流实验)
	// resetGameEnvironment()
	this.Data["pieceValue"] = 1
	this.Data["guid"] = GetGUID()
	this.Data["aboutInfo"] = aboutInfo
	this.TplNames = "QuanlifiedRateSinglePieceIndex.tpl"
}
func (this *GameController) QuanlifiedRateCompareIndex() {
	this.Data["guid"] = GetGUID()
	this.Data["aboutInfo"] = aboutInfo
	this.TplNames = "QuanlifiedRateCompareIndex.tpl"
}

// **********************************************************************

// **********************************************************************
// 实验公共部分
// *********************************
func (this *GameController) PlanList4Production() {
	// beego.Trace(g_planList4ProductionBalance)
	value := this.GetString("value")
	list := PlanList{}
	if iValue, err := strconv.Atoi(value); err == nil {
		beego.Trace("正在获取计划列表 => " + gameState(iValue).toString() + getFileLocation())
		list = g_PlanListMap[gameState(iValue)]
	} else {
		beego.Error("获取生产计划列表时出错 => " + gameState(iValue).toString() + getFileLocation())
	}
	this.Data["json"] = PlanList4DataTable{Data: list}
	this.ServeJson()
}
func (this *GameController) OrderCoverResults() {
	strState := this.GetString("state")
	if intState, err := strconv.Atoi(strState); err == nil {
		beego.Debug("正在获取 " + gameState(intState).toString() + " 订单完成率信息")
		if g := gameState(intState); g == 生产效率实验多件流 || g == 生产效率实验单件流 {
			if results, ok := g_OrderCoverResultsMap[g]; ok {
				beego.Emergency(results.printSelf())
				this.Data["json"] = results
			} else {
				beego.Error("OrderCoverResults => 数据不存在" + getFileLocation())
			}
		}
	} else {
		beego.Error("OrderCoverResults => 参数错误" + getFileLocation())
	}

	this.ServeJson()
}

func (this *GameController) Productivities() {
	strState := this.GetString("state")
	if intState, err := strconv.Atoi(strState); err == nil {
		if productivities, ok := g_ProductivitiesMap[gameState(intState)]; ok {
			this.Data["json"] = productivities
		} else {
			beego.Error("Productivities => 数据不存在" + getFileLocation())
		}
	} else {
		beego.Error("Productivities => 参数错误" + getFileLocation())
	}

	this.ServeJson()
}

func (this *GameController) SetNpiece() {
	strNvalue := this.GetString("Nvalue")
	if intNvalue, err := strconv.Atoi(strNvalue); err == nil {
		g_NpieceValue = intNvalue
		t := time.Now()
		originalTimeStamp = (t.Unix())
		// productivities.resetProductivity(g_NpieceValue)
		// resetProcessLoopNPieceValue(g_NpieceValue)
		beego.Debug("现在运行 " + strconv.Itoa(g_NpieceValue) + " 件流" + getFileLocation())
		this.Data["json"] = command{Code: 0, Message: ""}
	} else {
		this.Data["json"] = command{Code: 1, Message: "参数错误"}

	}
	// ServeJson:
	this.ServeJson()
}

func (this *GameController) AcceptSimulatorMessage() {
	// beego.Trace(string(this.Ctx.Input.RequestBody))
	name := this.GetString("name")
	state := this.GetString("state")
	btnCommand := BtnCommand{BtnName: name, State: state}
	updateProcessState(btnCommand)

	this.Data["json"] = command{Code: 0, Message: ""}
	this.ServeJson()
}
func (this *GameController) GameSimulatorIndex() {
	this.TplNames = "GameSimulatorIndex.tpl"
}
