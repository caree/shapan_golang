package controllers

import (
	"encoding/json"
	// "fmt"
	"github.com/astaxie/beego"
	"startbeego/models"
	// "strconv"
	"time"
	// "math/rand"
)

func newTimeLineMessage(msg string) TimeLineMessage {
	t := time.Now() //获取当前时间的结构体
	return TimeLineMessage{msg, t.Format("2006-01-02 15:04:05")}
}
func BroadcastBalanceMessage(results ProcessBalanceResults) {
	data, err := json.Marshal(results)
	if err != nil {
		beego.Error("Fail to marshal ProductionBalanceMessage:" + err.Error() + getFileLocation())
		return
	}
	broadcastWebSocket(newEvent(models.EVENT_BALANCE_STATISTIC_DATA, "", string(data)), "")

	// t := time.Now() //获取当前时间的结构体
	// return ProductionBalanceMessage{result, t.Format("2006-01-02 15:04:05")}

}
func BroadcastOrderCoverMessage(results OrderCoverResults) {
	data, err := json.Marshal(results)
	if err != nil {
		beego.Error("Fail to marshal BroadcastOrderCoverMessage:" + err.Error() + getFileLocation())
		return
	}
	broadcastWebSocket(newEvent(models.EVENT_ORDER_COVER_DATA, "", string(data)), "")
}
func BroadcastMessage_OrderCompleted() {
	broadcastWebSocket(newEvent(models.EVENT_ORDER_COMPLETED, "", ""), "")
}
func BroadcastMessage_PlanCompleted() {
	broadcastWebSocket(newEvent(models.EVENT_PLAN_COMPLETED, "", ""), "")
}
func BroadcastMessage_SysTip(msg string) {
	broadcastWebSocket(newEvent(models.EVENT_SYSTIP, "", msg), "")
}
func BroadcastProductivyMessage(productivities Productivities) {
	data, err := json.Marshal(productivities)
	if err != nil {
		beego.Error("Fail to marshal BroadcastProductivyMessage:" + err.Error() + getFileLocation())
		return
	}
	broadcastWebSocket(newEvent(models.EVENT_PRODUCTIVY_DATA, "", string(data)), "")
}

type TimeLineMessage struct {
	Message string
	Time    string
}

func (this TimeLineMessage) broadcastTimeLineMessage() {
	data, err := json.Marshal(this)
	if err != nil {
		beego.Error("Fail to marshal TimeLineMessage:" + err.Error() + getFileLocation())
		return
	}
	broadcastWebSocket(newEvent(models.EVENT_GAME_TIME_LINE, "", string(data)), TimeLineRequestURI_Prefix)
}

// type ProductionBalanceMessage struct {
// 	Result ProcessBalanceResult
// 	Time   string
// }
