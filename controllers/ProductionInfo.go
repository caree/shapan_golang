package controllers

import (
	// "encoding/json"
	"fmt"
	// "github.com/astaxie/beego"
	// "startbeego/models"
	// "strconv"
	// "time"
	// "math/rand"
	// "errors"
)

func init() {
	// beego.Error("init...")
	// pinfoList := &ProductionInfoList{}
	// for i := 0; i < len(g_productTypeList); i++ {
	// 	pinfoList.addNullInfo(g_productTypeList[i])
	// }
	// pinfoList.addProductCount(1, 1, 2, 3)
	// beego.Alert(pinfoList.printSelf())
	// beego.Error("init over")
}

type ProductionInfo struct {
	ProductTypeIndex               int
	ProductName                    string
	OnLineProductCount             int
	OffLineProductCount            int
	OffLineProductCountQcNotPassed int
}

func (this ProductionInfo) toString() string {
	return fmt.Sprintf("生产信息 =>  ProductTypeIndex: %d | ProductName: %s | OnLineProductCount: %d | OffLineProductCount: %d | OffLineProductCountQcNotPassed: %d",
		this.ProductTypeIndex, this.ProductName, this.OnLineProductCount, this.OffLineProductCount, this.OffLineProductCountQcNotPassed)
}
func (this *ProductionInfo) addProductCount(onLineProductCount, offLineProductCount, offLineProductCountQcNotPassed int) {
	if onLineProductCount >= 0 {
		this.OnLineProductCount += onLineProductCount
	}
	if offLineProductCount >= 0 {
		this.OffLineProductCount += offLineProductCount
	}
	if offLineProductCountQcNotPassed >= 0 {
		this.OffLineProductCountQcNotPassed += offLineProductCountQcNotPassed
	}
}

type ProductionInfoList []ProductionInfo

func (this *ProductionInfoList) addProductCount(productTypeIndex, onLineProductCount, offLineProductCount, offLineProductCountQcNotPassed int) {
	// beego.Debug(fmt.Sprintf("productTypeIndex: %d | onLineProductCount: %d | offLineProductCount: %d | offLineProductCountQcNotPassed:%d %s",
	// 	productTypeIndex, onLineProductCount, offLineProductCount, offLineProductCountQcNotPassed, getFileLocation()))
	for i := 0; i < len(*this); i++ {
		p := &((*this)[i])
		if p.ProductTypeIndex == productTypeIndex {
			p.addProductCount(onLineProductCount, offLineProductCount, offLineProductCountQcNotPassed)
			break
		}
	}
}
func (this *ProductionInfoList) addNullInfo(productType ProductType) {
	(*this) = append(*this, ProductionInfo{
		ProductTypeIndex:               productType.Index,
		ProductName:                    productType.Name,
		OnLineProductCount:             0,
		OffLineProductCount:            0,
		OffLineProductCountQcNotPassed: 0,
	})
}
func (this *ProductionInfoList) printSelf() string {
	str := ""
	for i := 0; i < len(*this); i++ {
		p := (*this)[i]
		str += "\r\n" + fmt.Sprintf("生产产品统计：%s => 在线半成品：%d  正常品：%d  不良品：%d", p.ProductName, p.OnLineProductCount, p.OffLineProductCount, p.OffLineProductCountQcNotPassed)
		// beego.Emergency(fmt.Sprintf("生产产品统计：%s => 在线半成品：%d  正常品：%d  不良品：%d", p.ProductName, p.OnLineProductCount, p.OffLineProductCount, p.OffLineProductCountQcNotPassed))
	}
	return str
}
