<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>生产效率实验订单列表</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" 
          type="image/png" 
          href="/images/logo_pure.png">
    <!-- Loading Bootstrap -->
    <!-- <link href="bootstrap/css/bootstrap.css" rel="stylesheet"> -->
    <link href="/bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">    
    <link rel="stylesheet" href="/bootstrap/css/font-awesome.min.css">
    <link href="/stylesheets/style.css" rel="stylesheet" media="screen">    
    <link href="/dataTable/jquery.dataTables.css" rel="stylesheet" media="screen">    
    <style type="text/css">

      #lblTip{
        margin-top: 30px; text-align: center; font-size: 30px; color: red; margin-bottom: 20px;
      }
      #dtProcess{
        text-align: center;
      }
      th{
        text-align: center;
      }
      .btn{
        width: 100px;
      }

    </style>
    <!-- // <script data-main="javascripts/main" src="/javascripts/require.js"></script> -->
    <script type="text/javascript" src = "/javascripts/jquery.js"></script>
    <script type="text/javascript" src = "/javascripts/underscore.js"></script>
    <script type="text/javascript">
        var wsUri = 'ws:' + window.location.href.substring(window.location.protocol.length) + '';
        var cmds = [];
        var table;

        $(document).ready(function() {
            table = $('#dtProcess').DataTable( {
                "paging":   false,
                "ordering": false,
                "info":     false,
                "searching":  false,
                "ajax": "../OrderList4ProductionEfficient?value=5",
                "columns": [
                    { "data": "index" ,"width": "25%"},
                    { "data": "name" ,"width": "25%"},
                    { "data": "quantity" ,"width": "25%"},
                    { "data": "time" ,"width": "25%"},
                ]
            } );
            $('#dtProcess tbody').on( 'click', 'tr', function () {
                if ( $(this).hasClass('selected') ) {
                    $(this).removeClass('selected');
                }
                else {
                    table.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                }
            } );
            // updateSaying();
            // setInterval(updateSaying, 30 * 1000);
        });
        function updateSaying(){
            $.get('../GetRandomSaying', function(_data){
                var saying = JSON.parse(_data);
                $('#sayingContent').text(saying.Content);
                $('#sayingAuthor').text(saying.Author);
            })
        }
        function openAboutWindow(){
          $('#modalAbout').modal();
        }    
        function start1(){
          window.open("../PlanList4ProductionEfficientNPieceIndex")
        }

        function start2(){
          window.open("../PlanList4ProductionEfficientSinglePieceIndex")
        }

        function hideTip(){
            $('#lblTip').css('display', 'none');
            $('#lblDeviceListText').css('margin-top', '40px');
        }
    </script>
  </head>
  <body>
    <div id = "divNav">
        <div class="container">
            <div class="row"  id = "">
                <div class="col-xs-1 col-sm-1 col-md-1 col-md-lg-1">
                    <img id="imglogo" src="/images/logo3.png">
                </div>
                <div class="col-xs-11 col-sm-11 col-md-11 col-md-lg-11">
                    <div style="">
                           <a class= "navFunc" style="right:6px;" onclick = "openAboutWindow()">关于 </a>
                     </div>
                </div>
            </div>
        </div>
    </div>    
<!--     <div class="demo-panel-title"> 
        <div class = "container">
            <div class="row" style="margin-top:20px;">
                <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12">
                    <div id= "sayingContent" style=""> </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12">
                    <div id="sayingAuthor" style=""></div>
                </div>
                
            </div>
        </div>
    </div> -->
    <div class="container">
        <div class="row" id = "subNavBar" style="">
            <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12">
                <div id="lblTableTitleText" style=""> 
                  <span class="glyphicon glyphicon-arrow-right" style="margin-right: 3px;"></span>
                  <span style="font-size: 16px;"> 生产效率实验订单列表 </span></div>
            </div>
         </div>
     </div>   

    <div class="container"  id= "mainContainer">
      <div class="row"  id = "row_container">
        <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12">
          <table id="dtProcess" class="display" cellspacing="0" width="100%" >
              <thead>
                  <tr>
                      <th>订单编码</th>
                      <th>产品名称</th>
                      <th>数量</th>
                      <th>时间</th>
                  </tr>
              </thead>
          </table>

        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12" style="margin-top: 50px;text-align:center;">
            <button type="button" class="btnAction" onclick= "start1()" style="">开始多件流</button>
            <button type="button" class="btnAction" onclick= "start2()" style="margin-left:50px">开始单件流</button>
            <!-- <button type="button" class="btn btn-default btn-sm" onclick= "showAddOrderIndex()">添加</button> -->
            <!-- <button type="button" class="btn btn-default btn-sm" onclick = "deleteRow()">删除</button> -->
            <!-- <button type="button" class="btn btn-default btn-sm" onclick = "showOrderAutoIndex()">批量生成</button> -->
        </div>
      </div> 
    </div>
<!--    <div class="container" style="margin-left: 0px; margin-right: 0px; max-width: 100%; background-color: rgb(240,240,240); max-width: 100%; min-height: 60px;position: absolute; width: 100%; bottom: 0px;">
      <div class="row"  id = "row_container">
            <div class="col-xs-3 col-sm-3 col-md-3 col-md-lg-3" style=""> </div>
          <div class="col-xs-6 col-sm-6 col-md-6 col-md-lg-6" style="text-align: center; font-size: 15px; margin-bottom: 0px; margin-top: 20px; color: rgb(180,180,180);">
              北京精益生产科技发展有限公司
          </div>
            <div class="col-xs-3 col-sm-3 col-md-3 col-md-lg-3" style=""> </div>
        
      </div>
    </div>  -->
    <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12" id = "tipContainer" style="">
            <div> 
                <span class="icon-pushpin" style="margin-right: 3px;"></span>
                <span style=""> 列表中列出了将要交付的订单 </span>
            </div>
          </div>
        </div>
    </div>      
    <script src="/bootstrap/js/bootstrap.min.js"></script>
    <script src="/layer/layer.min.js"></script>
    <script src="/dataTable/jquery.dataTables.js"></script>
    <!-- Modal -->
    <div class="modal fade" id="modalAbout" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header" style="background-color: rgb(50,50,50); color: white;">
            <button type="button" class="close" data-dismiss="modal"><!-- <span aria-hidden="true">&times;</span> --><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="myModalLabel">关于</h4>
          </div>
          <div class="modal-body">
            <div style="text-align: center; font-size: 19px; margin-top: 20px;">
              精益生产沙盘V0.8
            </div>
          </div>
          <div class="modal-footer" style="text-align: center;">
            <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
            <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
          </div>
        </div>
      </div>
    </div>
<!-- Modal -->
    <script language="javascript" type="text/javascript">
        function openAboutWindow(){
          $('#modalAbout').modal();
        }
    </script>
<div class="modal fade" id="modalAbout" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #00695c; color: white;">
        <button type="button" class="close" data-dismiss="modal"><!-- <span aria-hidden="true">&times;</span> --><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">关于</h4>
      </div>
      <div class="modal-body">
        <div style="text-align: center; font-size: 19px; margin-top: 20px;">
           {{.aboutInfo}}
        </div>
      </div>
      <div class="modal-footer" style="text-align: center;">
        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>
<!-- Modal End-->
  </body>
</html>
