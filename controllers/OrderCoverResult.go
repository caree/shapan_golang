package controllers

import (
	// "encoding/json"
	"fmt"
	// "github.com/astaxie/beego"
	// "startbeego/models"
	// "strconv"
	"time"
	// "math/rand"
)

type OrderCoverResult struct {
	GameStateFlag gameState
	Name          string
	CoverRate     int
	TimeElapse    int
}

func (this OrderCoverResult) toString() string {
	return fmt.Sprintf("订单完成率：GameStateFlag: %d | Name: %s | CoverRate: %d | TimeElapse: %d", this.GameStateFlag, this.Name, this.CoverRate, this.TimeElapse)
}

// func (this *OrderCoverResult) updateTimeElapse(_timeElapse int) {
// 	(*this).TimeElapse = _timeElapse
// }

type OrderCoverResults []OrderCoverResult

func (this *OrderCoverResults) addResult(state gameState, name string, rate int) OrderCoverResult {
	t := time.Now()
	result := OrderCoverResult{
		GameStateFlag: state,
		Name:          name,
		CoverRate:     rate,
		TimeElapse:    int(t.Unix() - originalTimeStamp),
	}
	(*this) = append((*this), result)
	return result
}

func (this *OrderCoverResults) printSelf() string {
	str := ""
	for i := 0; i < len(*this); i++ {
		result := (*this)[i]
		str += "\r\n" + result.toString()
		// beego.Trace(result.toString())
	}
	return str
}
func (this *OrderCoverResults) clearResults() {
	(*this) = OrderCoverResults{}
}
