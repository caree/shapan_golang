package routers

import (
	"github.com/astaxie/beego"
	"startbeego/controllers"
)

func init() {
	beego.Router("/", &controllers.MainController{}, "get:Index")
	beego.Router("/GetRandomSaying", &controllers.MainController{}, "get:GetRandomSaying")
	beego.Router("/StartGameIndex", &controllers.MainController{}, "get:StartGameIndex")

	beego.Router("/planConfigIndex", &controllers.PlanController{}, "get:PlanConfigIndex")
	beego.Router("/addPlanIndex", &controllers.PlanController{}, "get:AddPlanIndex")
	// beego.Router("/addPlan", &controllers.PlanController{}, "post:AddPlan")
	// beego.Router("/deletePlan", &controllers.PlanController{}, "post:DeletePlan")

	beego.Router("/planConfigIndexws", &controllers.WebSocketController{}, "get:Join")
	beego.Router("/processConfigIndexws", &controllers.WebSocketController{}, "get:Join")
	beego.Router("/addProcessIndexws", &controllers.WebSocketController{}, "get:Join")
	beego.Router("/timelineIndexws", &controllers.WebSocketController{}, "get:Join")
	beego.Router("/NpieceFlowIndexws", &controllers.WebSocketController{}, "get:Join")
	beego.Router("/BalanceProductionIndexws", &controllers.WebSocketController{}, "get:Join")
	beego.Router("/QuanlifiedRateIndexws", &controllers.WebSocketController{}, "get:Join")
	beego.Router("/QuanlifiedRateNPieceIndexws", &controllers.WebSocketController{}, "get:Join")
	beego.Router("/QuanlifiedRateSinglePieceIndexws", &controllers.WebSocketController{}, "get:Join")
	beego.Router("/ProductionEfficientNpieceFlowIndexws", &controllers.WebSocketController{}, "get:Join")
	beego.Router("/ProductionEfficientSinglePieceFlowIndexws", &controllers.WebSocketController{}, "get:Join")

	// beego.Router("/orderConfigIndex", &controllers.OrderController{}, "get:OrderConfigIndex")
	// beego.Router("/addOrderIndex", &controllers.OrderController{}, "get:AddOrderIndex")
	// beego.Router("/orderList", &controllers.OrderController{}, "get:OrderList")
	beego.Router("/productTypeList", &controllers.OrderController{}, "get:ProductTypeList")
	// beego.Router("/addOrder", &controllers.OrderController{}, "post:AddOrder")
	// beego.Router("/deleteOrder", &controllers.OrderController{}, "post:DeleteOrder")

	beego.Router("/processConfigIndex", &controllers.ProcessController{}, "get:ProcessConfigIndex")
	beego.Router("/addProcessIndex", &controllers.ProcessController{}, "get:AddProcessIndex")
	beego.Router("/timelineIndex", &controllers.ProcessController{}, "get:TimelineIndex")
	beego.Router("/processList", &controllers.ProcessController{}, "get:ProcessList")
	beego.Router("/addProcess", &controllers.ProcessController{}, "post:AddProcess")
	beego.Router("/deleteProcessConfig", &controllers.ProcessController{}, "post:DeleteProcess")

	beego.Router("/GameSimulatorIndex", &controllers.GameController{}, "get:GameSimulatorIndex")
	beego.Router("/AcceptSimulatorMessage", &controllers.GameController{}, "post:AcceptSimulatorMessage")
	beego.Router("/OrderList4ProductionEfficientIndex", &controllers.GameController{}, "get:OrderList4ProductionEfficientIndex")
	beego.Router("/OrderList4ProductionEfficient", &controllers.GameController{}, "get:OrderList4ProductionEfficient")
	beego.Router("/PlanList4ProductionEfficientNPieceIndex", &controllers.GameController{}, "get:PlanList4ProductionEfficientNPieceIndex")
	beego.Router("/PlanList4ProductionEfficientSinglePieceIndex", &controllers.GameController{}, "get:PlanList4ProductionEfficientSinglePieceIndex")
	beego.Router("/PlanList4ProductionEfficient", &controllers.GameController{}, "get:PlanList4ProductionEfficient")
	beego.Router("/PlanList4BalanceIndex", &controllers.GameController{}, "get:PlanList4BalanceIndex")
	beego.Router("/PlanList4QuanlifiedProductionNPieceIndex", &controllers.GameController{}, "get:PlanList4QuanlifiedProductionNPieceIndex")
	beego.Router("/PlanList4QuanlifiedProductionSinglePieceIndex", &controllers.GameController{}, "get:PlanList4QuanlifiedProductionSinglePieceIndex")
	beego.Router("/PlanList4Production", &controllers.GameController{}, "get:PlanList4Production")
	beego.Router("/BalanceProductionIndex", &controllers.GameController{}, "get:BalanceProductionIndex")
	// beego.Router("/NpieceFlowIndex", &controllers.GameController{}, "get:NpieceFlowIndex")
	// beego.Router("/QuanlifiedRateIndex", &controllers.GameController{}, "get:QuanlifiedRateIndex")
	beego.Router("/QuanlifiedRateNPieceIndex", &controllers.GameController{}, "get:QuanlifiedRateNPieceIndex")
	beego.Router("/QuanlifiedRateSinglePieceIndex", &controllers.GameController{}, "get:QuanlifiedRateSinglePieceIndex")
	beego.Router("/QuanlifiedRateCompareIndex", &controllers.GameController{}, "get:QuanlifiedRateCompareIndex")
	beego.Router("/SetNpiece", &controllers.GameController{}, "post:SetNpiece")
	beego.Router("/Productivities", &controllers.GameController{}, "get:Productivities")
	beego.Router("/OrderCoverResults", &controllers.GameController{}, "get:OrderCoverResults")
	beego.Router("/ProductionEfficientSinglePieceFlowIndex", &controllers.GameController{}, "get:ProductionEfficientSinglePieceFlowIndex")
	beego.Router("/ProductionEfficientNpieceFlowIndex", &controllers.GameController{}, "get:ProductionEfficientNpieceFlowIndex")

}
