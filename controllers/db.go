package controllers

import (
	"database/sql"
	"fmt"
	"github.com/astaxie/beego"
	_ "github.com/mattn/go-sqlite3"
)

var guid = 1

func GetGUID() int {
	guid = guid + 1
	return guid
}

var db *sql.DB

func init() {
	// fmt.Println("db init")
	var err error
	db, err = sql.Open("sqlite3", "./db.db3")
	checkErr(err)

	beego.Emergency("数据库初始化成功!!!!")
	// db.Close()
}
func closeDB() {
	db.Close()
	beego.Emergency("DB closed success!!!!")
}

func dbdemo() {
	//插入数据
	stmt, err := db.Prepare("INSERT INTO userinfo(username, departname, created) values(?,?,?)")
	checkErr(err)

	res, err := stmt.Exec("zhang", "ceo", "2014-12-09")
	checkErr(err)
	fmt.Println(res)

	stmt, err = db.Prepare("INSERT INTO userinfo(username, departname, created) values(?,?,?)")
	checkErr(err)
	res, err = stmt.Exec("astaxie", "研发部门", "2012-12-09")
	checkErr(err)
	fmt.Println(res)

	id, err := res.LastInsertId()
	checkErr(err)
	fmt.Printf("insert affect row id = %d\n", id)
	// fmt.Println(id)

	//更新数据
	stmt, err = db.Prepare("update userinfo set username=? where uid=?")
	checkErr(err)

	res, err = stmt.Exec("astaxieupdate", id)
	checkErr(err)
	affect, err := res.RowsAffected()
	checkErr(err)

	fmt.Printf("update affect %d row(s)\n", affect)

	//查询数据
	rows, err := db.Query("SELECT * FROM userinfo")
	checkErr(err)

	fmt.Println("select users: ")
	for rows.Next() {
		var uid int
		var username string
		var department string
		var created string
		err = rows.Scan(&uid, &username, &department, &created)
		checkErr(err)
		fmt.Print(uid)
		fmt.Print(" " + username)
		fmt.Print(" " + department)
		fmt.Println(" " + created)
	}

	//删除数据
	stmt, err = db.Prepare("delete from userinfo where uid=?")
	checkErr(err)

	res, err = stmt.Exec(id)
	checkErr(err)

	affect, err = res.RowsAffected()
	checkErr(err)

	fmt.Println(affect)

	stmt, err = db.Prepare("delete from userinfo")
	checkErr(err)

	res, err = stmt.Exec()
	checkErr(err)
}
func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}
