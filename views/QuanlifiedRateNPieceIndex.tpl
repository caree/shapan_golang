<!DOCTYPE html>
<meta charset="utf-8" />
<title>良品率实验多件流</title>
    <link rel="icon" 
          type="image/png" 
          href="/images/logo_pure.png">
    <link href="/bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">    
    <link rel="stylesheet" href="/bootstrap/css/font-awesome.min.css">
    <link href="/stylesheets/style.css" rel="stylesheet" media="screen">    
    <link rel='stylesheet' href='/stylesheets/c3.css' />
    <script type="text/javascript" src = "/javascripts/jquery.js"></script>
    <script type="text/javascript" src = "/javascripts/d3.js"></script>
    <script type="text/javascript" src = "/javascripts/c3.js"></script>
    <script type="text/javascript" src = "/javascripts/underscore.js"></script>

<script language="javascript" type="text/javascript">
    // var wsUri = "ws://localhost:3003/";
	var wsUri = 'ws:' + window.location.href.substring(window.location.protocol.length)+ 'ws?guid={{.guid}}';
    var chart;
    var nPieceFlowMessages = []
    var chartStyle = "null"
    var output;
    function init() {
        output = document.getElementById("output");
        setToLineChart()
        createWebSocket();
        setInterval(checkConnectionStatus, 5*1000)            
        // submitSelect(function(){
           
        // })
                   
    }
    function setToPieChart(){
        if(chartStyle == 'pie'){
            return
        }
        chartStyle = 'pie'
        chart = c3.generate({
            data: {
                // iris data from R
                columns: buildPieChartColumnData(),
                type : 'pie',
            }
        });
    }
    function setToLineChart(){
        if(chartStyle == 'line') return
        chartStyle = 'line'
        chart = c3.generate({
            data: {
                    xs: {
                        '三件流': 'x1',
                        '单件流': 'x2',
                    },
                    columns: buildLineChartData(),
                    type: 'spline',
                    // [
                    //     ['x1', 0],
                    //     ['x2', 0],
                    //     ['三件流', 0],
                    //     ['单件流', 0]
                    // ]
            },
            grid: {
                    x: {
                        show: true
                    },
                    y: {
                        show: true
                    }
            },             
            point: { r: 5 },
            axis: {
                x: {
                    label: {
                        text: '时间',
                        position: 'outer-right',
                    }
                },
                y: {
                    label: {
                        text: '良品率',
                        position: 'outer-middle',
                    }
                },
            },             
        });  
    }
    function submitSelect(callback){
        var pieceValue = {{.pieceValue}}
        $.post('../SetNpiece', {Nvalue: pieceValue}, function(data){
            if(data.code == 0){
              console.log('设置成功')
              if(callback != null){
                callback()
              }
            }else if(data.code == 1){
              layer.alert(data.message);
            }
        })     
    }
    function buildPieChartColumnData(){
        console.log("组织饼状图数据 =>")
        if(_.size(nPieceFlowMessages) <= 0){
            var rtn = [['合格品', 10], ['不良品', 15]]
            console.log(JSON.stringify(rtn))
            return rtn
        }
        var len = _.size(nPieceFlowMessages)
        var quanlified = ['合格品', nPieceFlowMessages[len-1].OffLineProductCount]
        var unquanlified = ['不良品', nPieceFlowMessages[len-1].OffLineProductCountQcNotPassed]
        // _.each(nPieceFlowMessages, function(_message){
        //     quanlified.push(_message.OffLineProductCount)
        //     unquanlified.push(_message.OffLineProductCountQcNotPassed)
        // }) 
        var rtn = [quanlified, unquanlified]
        console.log(JSON.stringify(rtn))
        return  rtn               
    }
    function buildLineChartData(){
        console.log("组织线形图数据 =>")
        if(_.size(nPieceFlowMessages) <= 0){
            var rtn = [['x1', 0,10,15], ['x2', 0, 5, 10], ['三件流', 0,10, 15], ['单件流', 0, 1, 2]]
            console.log(JSON.stringify(rtn))
            return rtn
        }
        var x1 = ['x1',0]
        var three = ['三件流',0]
        
        var x2 = ['x2',0]
        var single = ['单件流',0]

        _.each(nPieceFlowMessages, function(_message){
            if(_message.GameStateFlag == 3){//多件流
                x1.push(_message.Timestamp)
                three.push(_message.OffLineProductCountQcNotPassed)
            }else if(_message.GameStateFlag == 4){//单件流
                x2.push(_message.Timestamp)
                single.push(_message.OffLineProductCountQcNotPassed)
            }
        }) 
        var rtn = [x1, x2, three, single]
        console.log(JSON.stringify(rtn))
        return  rtn
    }
    function updateNPieceFlowMessage(message){
        // [{"GameStateFlag":0,"Time":"13:06:24","Timestamp":21,"OnLineProductCount":0,"OffLineProductCount":0,"OffLineProductCountQcNotPassed":3,"ProductTypeIndex":0,"ProductName":"型号一"},{"GameStateFlag":0,"Time":"13:06:24","Timestamp":21,"OnLineProductCount":0,"OffLineProductCount":0,"OffLineProductCountQcNotPassed":0,"ProductTypeIndex":1,"ProductName":"型号二"},{"GameStateFlag":0,"Time":"13:06:24","Timestamp":21,"OnLineProductCount":0,"OffLineProductCount":0,"OffLineProductCountQcNotPassed":0,"ProductTypeIndex":2,"ProductName":"型号三"}]
        console.log("接收到信息如下：")
        if(_.isArray(message)){
            for (var i = message.length - 1; i >= 0; i--) {
                var m = message[i]
                console.log("GameStateFlag: %d | OnLineProductCount: %d | OffLineProductCount: %d | OffLineProductCountQcNotPassed: %d", m.GameStateFlag, m.OnLineProductCount, m.OffLineProductCount, m.OffLineProductCountQcNotPassed)
                nPieceFlowMessages.push(m)
            };
        }
        // return
        // nPieceFlowMessages.push(message)
        // {"NpieceValue":3,"Time":"09:42:36","Timestamp":19,"OnLineProductCount":3,"OffLineProductCount":0,"OffLineProductCountQcNotPassed":0}
        if(chartStyle == 'pie'){
            chart.load({
                columns: buildPieChartColumnData(),
            });
        }else{
            chart.load({
                columns: buildLineChartData(),
            });
        }

    }
    function startNextStep(){
        window.location.href="../PlanList4QuanlifiedProductionSinglePieceIndex"
    }
    function createWebSocket() {
        websocket = new WebSocket(wsUri);
        websocket.onopen = function (evt) { onOpen(evt) };
        websocket.onclose = function (evt) { onClose(evt) };
        websocket.onmessage = function (evt) { onMessage(evt) };
        websocket.onerror = function (evt) { onError(evt) };
    }
    function onOpen(evt) {
        writeToScreen("已连接到服务器");
    }
    function onClose(evt) {
        writeToScreen("连接已断开");
        websocket=null
    }
    function onMessage(evt) {
        // writeToScreen('<span style="color: blue;">RESPONSE: ' + evt.data+'</span>');
        var cmd = JSON.parse(evt.data);
        switch(cmd.Type){
            case 4:
                writeToScreen(cmd.Content)
                break
            case 6:
                console.log(cmd.Content)
                updateNPieceFlowMessage(JSON.parse(cmd.Content))
                break
            case 7:
                startNextStep()
            break
        }
    }
    function onError(evt) {
        writeToScreen('<span style="color: red;">ERROR:</span> ' + evt.data);
        websocket=null
    }
    function doSend(message) {
        writeToScreen("SENT: " + message);
        websocket.send(message);
    }
    function writeToScreen(message) {
        var pre = document.createElement("p");
        pre.style.wordWrap = "break-word";
        pre.innerHTML = message;
        output.appendChild(pre);
    }
    function checkConnectionStatus(){
      if(websocket == null){
        createWebSocket()
      }
    }    
    window.addEventListener("load", init, false);
</script>

<!-- <h2>N件流</h2> -->
<body> 

    <div id = "divNav">
        <div class="container">
            <div class="row"  id = "">
                <div class="col-xs-1 col-sm-1 col-md-1 col-md-lg-1">
                    <img id="imglogo" src="/images/logo3.png">
                </div>
                <div class="col-xs-11 col-sm-11 col-md-11 col-md-lg-11">
                    <div style="">
                           <a class= "navFunc" style="right:6px;" onclick = "openAboutWindow()">关于 </a>
                     </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row" id = "subNavBar" style="">
            <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12">
                <div id="lblTableTitleText" style=""> 
                  <span class="glyphicon glyphicon-arrow-right" style="margin-right: 3px;"></span>
                  <span style="font-size: 16px;"> 不良品实验统计图 </span></div>
            </div>
         </div>
     </div>        
    <div class="container" >
        <div class="row chartOutterContainer"  id = "row_container">
            <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12" style="margin-top:5px;">
                <div id="chart" style=""  class="chartInnerContainer"></div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12" style="margin-top: 30px;margin-bottom: 10px;text-align:center;">
                <button type="button" class="btnMayDo" style="" onclick = "setToLineChart()">不良品量统计图</button>
                <button type="button" class="btnMayDo" style="" onclick = "setToPieChart()">良品率统计图</button>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12" id = "tipContainer" style="">
            <div> 
                <span class="icon-pushpin" style="margin-right: 3px;"></span>
                <span style=""> 上图展示了实验过程中在特定时间产生的不良品数量 </span>
            </div>
          </div>
        </div>
    </div>  
<!-- Modal -->
    <script language="javascript" type="text/javascript">
        function openAboutWindow(){
          $('#modalAbout').modal();
        }
    </script>
<div class="modal fade" id="modalAbout" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #00695c; color: white;">
        <button type="button" class="close" data-dismiss="modal"><!-- <span aria-hidden="true">&times;</span> --><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">关于</h4>
      </div>
      <div class="modal-body">
        <div style="text-align: center; font-size: 19px; margin-top: 20px;">
           {{.aboutInfo}}
        </div>
      </div>
      <div class="modal-footer" style="text-align: center;">
        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>
<!-- Modal End-->
    <script src="/bootstrap/js/bootstrap.min.js"></script>
</body>
<div id="output"></div>
</html>
  
  