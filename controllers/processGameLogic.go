package controllers

import (
	// "encoding/json"
	"fmt"
	"github.com/astaxie/beego"
	// "startbeego/models"
	// "strconv"
	"time"
	// "math/rand"
	"runtime"
	"strings"
)

const DEFAULT_NPIECE_VALUE int = 0

var g_NpieceValue int = DEFAULT_NPIECE_VALUE //批量大小
var 运行实验状态 gameState = 未设定
var originalTimeStamp int64 = 0 //开始计时的零值
var g_processLoopListMap ProcessLoopListMap
var g_ProductivitiesMap ProductivitiesMap
var g_ProcessBalanceResultsMap ProcessBalanceResultsMap
var g_PlanListMap PlanListMap
var g_orderListMap OrderListMap
var g_OrderCoverResultsMap OrderCoverResultsMap

// var processLoopList ProcessLoopList
// var productivities Productivities               //产量统计
// var processBalanceResults ProcessBalanceResults //生产平衡结果统计
// var currentPlanList PlanList

var totalProcess ProductionProcessState

// var processStateList ProcessStateList //流程信息统计

//******************************************************************************
type gameState int
type ProcessLoopListMap map[gameState]ProcessLoopList
type ProductivitiesMap map[gameState]Productivities
type ProcessBalanceResultsMap map[gameState]ProcessBalanceResults
type PlanListMap map[gameState]PlanList
type OrderListMap map[gameState]OrderList
type OrderCoverResultsMap map[gameState]OrderCoverResults

// var g_orderList = OrderList{
// 	ProductionOrder{Name: "型号一", Quantity: 2, Completed: false, Index: 1, Time: "", ProductTypeIndex: 0},
// 	ProductionOrder{Name: "型号二", Quantity: 2, Completed: false, Index: 2, Time: "", ProductTypeIndex: 1},
// 	ProductionOrder{Name: "型号三", Quantity: 2, Completed: false, Index: 2, Time: "", ProductTypeIndex: 2},
// }
var g_productTypeList = []ProductType{
	ProductType{Index: 0, Name: "型号一"},
	ProductType{Index: 1, Name: "型号二"},
	ProductType{Index: 2, Name: "型号三"},
}

var g_productionBalance_PieceValue = 2

// var g_planList4ProductionBalance = PlanList{ //生产平衡实验计划列表
// 	// ProductionPlan{Index: 1, Name: "产品一", Quantity: g_productionBalance_PieceValue, Completed: false, ProductTypeIndex: 1},
// 	// ProductionPlan{Index: 2, Name: "产品一", Quantity: g_productionBalance_PieceValue, Completed: false, ProductTypeIndex: 1},
// 	// ProductionPlan{Index: 3, Name: "产品一", Quantity: g_productionBalance_PieceValue, Completed: false, ProductTypeIndex: 1},
// 	// ProductionPlan{Index: 4, Name: "产品一", Quantity: g_productionBalance_PieceValue, Completed: false, ProductTypeIndex: 1},
// 	// ProductionPlan{Index: 5, Name: "产品一", Quantity: g_productionBalance_PieceValue, Completed: false, ProductTypeIndex: 1},
// }
var g_planList4QuanlifiedProductionNPiece_PieceValue = 3

// var g_planList4ProductionQuanlifiedNPiece = PlanList{ //不良品实验多件流计划列表
// 	// ProductionPlan{Index: 1, Name: "产品一", Quantity: g_planList4QuanlifiedProductionNPiece_PieceValue, Completed: false, ProductTypeIndex: 1},
// 	// ProductionPlan{Index: 2, Name: "产品一", Quantity: g_planList4QuanlifiedProductionNPiece_PieceValue, Completed: false, ProductTypeIndex: 1},
// 	// ProductionPlan{Index: 3, Name: "产品一", Quantity: g_planList4QuanlifiedProductionNPiece_PieceValue, Completed: false, ProductTypeIndex: 1},
// 	// ProductionPlan{Index: 4, Name: "产品一", Quantity: g_planList4QuanlifiedProductionNPiece_PieceValue, Completed: false, ProductTypeIndex: 1},
// 	// ProductionPlan{Index: 5, Name: "产品一", Quantity: g_planList4QuanlifiedProductionNPiece_PieceValue, Completed: false, ProductTypeIndex: 1},
// }

// var g_planList4ProductionQuanlifiedSinglePiece, g_planList4ProductionEfficientNpiece, g_planList4ProductionEfficientSinglePiece = PlanList{ //不良品实验单件流计划列表
// ProductionPlan{Index: 1, Name: "产品一", Quantity: 1, Completed: false, ProductTypeIndex: 1},
// ProductionPlan{Index: 2, Name: "产品一", Quantity: 1, Completed: false, ProductTypeIndex: 1},
// ProductionPlan{Index: 3, Name: "产品一", Quantity: 1, Completed: false, ProductTypeIndex: 1},
// ProductionPlan{Index: 4, Name: "产品一", Quantity: 1, Completed: false, ProductTypeIndex: 1},
// ProductionPlan{Index: 5, Name: "产品一", Quantity: 1, Completed: false, ProductTypeIndex: 1},
// ProductionPlan{Index: 6, Name: "产品一", Quantity: 1, Completed: false, ProductTypeIndex: 1},
// }, PlanList{}, PlanList{}

const (
	未设定       = 1 //0
	生产平衡实验    = 2 //2
	良品率多件流实验  = 3 //3
	良品率单件流实验  = 4 //4
	生产效率实验多件流 = 5 //5
	生产效率实验单件流 = 6 //6
)

func (this gameState) toString() string {
	str := ""
	switch this {
	case 未设定:
		str = "未设定"
	case 生产效率实验多件流:
		str = "生产效率实验多件流"
	case 生产效率实验单件流:
		str = "生产效率实验单件流"
	case 生产平衡实验:
		str = "生产平衡实验"
	case 良品率多件流实验:
		str = "良品率多件流实验"
	case 良品率单件流实验:
		str = "良品率单件流实验"
	}
	return str
}

func init() {
	// restartGame()
	// g_planList4ProductionQuanlifiedSinglePiece = g_planList4ProductionQuanlifiedNPiece.splitToSinglePiecePlan()
	// g_planList4ProductionEfficientNpiece = g_orderList.toProductionPlans(3)
	// g_planList4ProductionEfficientSinglePiece = g_orderList.toProductionPlans(1)

}
func restartGame() {
	beego.Alert("重新开始整个实验。。。")
	g_processLoopListMap = make(ProcessLoopListMap)
	g_ProductivitiesMap = make(ProductivitiesMap)
	g_ProcessBalanceResultsMap = make(ProcessBalanceResultsMap)
	g_PlanListMap = make(PlanListMap)
	g_orderListMap = make(OrderListMap)
	g_OrderCoverResultsMap = make(OrderCoverResultsMap)

	// 订单系统
	orderList := OrderList{
	// ProductionOrder{Name: "型号一", Quantity: 2, Completed: false, Index: 1, Time: "", ProductTypeIndex: 0},
	// ProductionOrder{Name: "型号二", Quantity: 2, Completed: false, Index: 2, Time: "", ProductTypeIndex: 1},
	// ProductionOrder{Name: "型号三", Quantity: 2, Completed: false, Index: 2, Time: "", ProductTypeIndex: 2},
	}
	orderList.addNewOrder(g_productTypeList[0], 2)
	orderList.addNewOrder(g_productTypeList[1], 2)
	orderList.addNewOrder(g_productTypeList[2], 2)

	g_orderListMap[生产效率实验多件流] = append(OrderList{}, orderList...)
	g_orderListMap[生产效率实验单件流] = append(OrderList{}, orderList...)
	if list, ok := g_orderListMap[生产效率实验多件流]; ok {
		beego.Debug("生产效率实验多件流 订单列表：" + getFileLocation())
		beego.Emergency(list.printSelf())
	} else {
		beego.Error("生产效率实验多件流订单列表获取失败" + getFileLocation())
	}
	if list, ok := g_orderListMap[生产效率实验单件流]; ok {
		beego.Debug("生产效率实验单件流 订单列表：" + getFileLocation())
		beego.Emergency(list.printSelf())
	} else {
		beego.Error("生产效率实验单件流订单列表获取失败" + getFileLocation())
	}
	// 订单完成结果记录系统
	g_OrderCoverResultsMap[生产效率实验多件流] = OrderCoverResults{}
	g_OrderCoverResultsMap[生产效率实验单件流] = OrderCoverResults{}

	//计划系统
	g_PlanListMap[未设定] = PlanList{}
	resetPlan4Balance()
	resetPlan4良品率多件流实验()
	resetPlan4良品率单件流实验()
	resetPlan4生产效率实验多件流()
	resetPlan4生产效率实验单件流()

	resetGameRealtimeRecordEnvironment(未设定)
}

// **********************************************************************

func resetGameRealtimeRecordEnvironment(state gameState) {
	g_processLoopListMap[state] = ProcessLoopList{}
	g_ProductivitiesMap[state] = Productivities{}
	g_ProcessBalanceResultsMap[state] = ProcessBalanceResults{}
	g_OrderCoverResultsMap[state] = OrderCoverResults{}

	t := time.Now()
	originalTimeStamp = t.Unix()
}

func updateProcessState(btnCommand BtnCommand) {
	if 运行实验状态 == 未设定 {
		beego.Alert("运行实验状态==未设定" + getFileLocation())
		return
	}
	processLoopList, ok := g_processLoopListMap[运行实验状态]
	if !ok {
		beego.Error("updateProcessState => 1 系统运行错误" + getFileLocation())
		return
	}
	updated, loopEnded, loop := processLoopList.updateProcessLoop(btnCommand)
	g_processLoopListMap[运行实验状态] = processLoopList
	if updated {
		// beego.Trace("系统信息已更新")
		if btnCommand.closeNormal() || btnCommand.closeYAS() {
			// produceResult(loop)
			err, result := loop.countProcessTimeElapse()
			if err == nil {
				if processBalanceResults, ok := g_ProcessBalanceResultsMap[运行实验状态]; ok {
					processBalanceResults.addResult(result)
					g_ProcessBalanceResultsMap[运行实验状态] = processBalanceResults
				} else {
					beego.Error("updateProcessState => 2 系统运行错误" + getFileLocation())
				}
			} else {
				beego.Error("添加生产平衡记录时出现错误" + getFileLocation())
			}
			if btnCommand.closeYAS() {
				str := "产品质量经检验不合格，当前在线半成品均标记为不合格品，请调整产线重新开始生产"
				beego.Alert(str + getFileLocation())
				BroadcastMessage_SysTip(str)
				//将当前已经开始的流程全部标记为结束
				setAllOnlineProductsUnquanlified()
			}
		}

		if loopEnded {
			beego.Alert("生产批次结束" + getFileLocation())
			completeSomePlan(1) //完成一个计划
			beego.Debug("批次列表信息：" + getFileLocation())
			beego.Emergency(processLoopList.printSelf())
			//
			productionInfoList := processLoopList.countProductCount()
			beego.Alert("一个生产批次完成，生产结果如下：" + getFileLocation())
			beego.Emergency(productionInfoList.printSelf())
			if productivities, ok := g_ProductivitiesMap[运行实验状态]; ok {
				pro := productivities.addProductivity(productionInfoList, 运行实验状态)
				BroadcastProductivyMessage(pro)
				g_ProductivitiesMap[运行实验状态] = productivities
			} else {
				beego.Error("updateProcessState => 3 系统运行错误" + getFileLocation())
			}
			// 更新订单完成情况
			if updateOrderCompletation(productionInfoList) {
				setRunningState(未设定) //设置系统不接收信号
			}
		}

	} else {
		if processLoopList.hasNullLoop() == false { //如果因为没有空循环而无法消费按钮信息，则需要添加新循环
			// fmt.Println("hasNullLoop => false")
			// fmt.Println("before =>  " + fmt.Sprintf("%+v", processLoopList))

			// 如果还有生产计划，添加新循环否则不添加了
			if planListTemp, ok := g_PlanListMap[运行实验状态]; ok {
				if has, plan := planListTemp.hasUncompletedPlan(); has {
					processLoopList.addNewProcessLoop(plan.Quantity, plan.ProductTypeIndex)
					// processLoopList.addNewProcessLoop(g_NpieceValue, plan.ProductTypeIndex)
					// fmt.Println("after =>  " + fmt.Sprintf("%+v", processLoopList))
					g_processLoopListMap[运行实验状态] = processLoopList
					updateProcessState(btnCommand)
					// processLoopList.printSelf()
				} else {
					beego.Alert("没有生产计划了" + getFileLocation())
				}
			} else {
				beego.Error("updateProcessState => 3 系统运行错误" + getFileLocation())
			}
		} else {
			beego.Alert("系统接收到无用的按钮触发信息" + getFileLocation())
		}
	}
	// beego.Debug(processLoopList.printSelf())
}
func resetPlan4生产效率实验多件流() {
	list := g_orderListMap[生产效率实验多件流].toProductionPlans(3)
	g_PlanListMap[生产效率实验多件流] = list
	beego.Debug("生产效率实验多件流 计划列表:" + getFileLocation())
	beego.Emergency(list.printSelf())
}
func resetPlan4生产效率实验单件流() {
	list := g_orderListMap[生产效率实验单件流].toProductionPlans(1)
	g_PlanListMap[生产效率实验单件流] = list
	beego.Debug("生产效率实验单件流 计划列表:" + getFileLocation())
	beego.Emergency(list.printSelf())
}
func resetPlan4Balance() {
	planList4ProductionBalance := PlanList{}
	planList4ProductionBalance.addNewPlan(g_productTypeList[0], g_productionBalance_PieceValue)
	planList4ProductionBalance.addNewPlan(g_productTypeList[0], g_productionBalance_PieceValue)
	// planList4ProductionBalance.addNewPlan(g_productTypeList[0], g_productionBalance_PieceValue)
	// planList4ProductionBalance.addNewPlan(g_productTypeList[0], g_productionBalance_PieceValue)
	// planList4ProductionBalance.addNewPlan(g_productTypeList[0], g_productionBalance_PieceValue)
	g_PlanListMap[生产平衡实验] = planList4ProductionBalance
	if list, ok := g_PlanListMap[生产平衡实验]; ok {
		beego.Debug("生产平衡实验计划列表:" + getFileLocation())
		beego.Emergency(list.printSelf())
	} else {
		beego.Error("生产平衡实验计划列表获取失败" + getFileLocation())
	}
}
func resetPlan4良品率多件流实验() {
	planList4ProductionQuanlifiedNPiece := PlanList{}
	planList4ProductionQuanlifiedNPiece.addNewPlan(g_productTypeList[0], g_planList4QuanlifiedProductionNPiece_PieceValue)
	planList4ProductionQuanlifiedNPiece.addNewPlan(g_productTypeList[0], g_planList4QuanlifiedProductionNPiece_PieceValue)
	// planList4ProductionQuanlifiedNPiece.addNewPlan(g_productTypeList[0], g_planList4QuanlifiedProductionNPiece_PieceValue)
	// planList4ProductionQuanlifiedNPiece.addNewPlan(g_productTypeList[0], g_planList4QuanlifiedProductionNPiece_PieceValue)
	// planList4ProductionQuanlifiedNPiece.addNewPlan(g_productTypeList[0], g_planList4QuanlifiedProductionNPiece_PieceValue)
	g_PlanListMap[良品率多件流实验] = planList4ProductionQuanlifiedNPiece
	beego.Debug("设置良品率多件流实验生产计划：" + getFileLocation())
	beego.Emergency(planList4ProductionQuanlifiedNPiece.printSelf())
	resetPlan4良品率单件流实验()
}
func resetPlan4良品率单件流实验() {
	if list, ok := g_PlanListMap[良品率多件流实验]; ok {
		g_PlanListMap[良品率单件流实验] = list.splitToSinglePiecePlan()
		beego.Debug("设置良品率单件流实验生产计划：" + getFileLocation())
		beego.Emergency(list.splitToSinglePiecePlan().printSelf())
	} else {
		beego.Error("设置良品率单件流实验时出错" + getFileLocation())
	}
}
func setRunningState(state gameState) {
	beego.Debug("当前状态：" + state.toString() + getFileLocation())
	运行实验状态 = state
	resetGameRealtimeRecordEnvironment(state)

	switch state {
	case 未设定:
	case 生产效率实验多件流:
		resetPlan4生产效率实验多件流()
	case 生产效率实验单件流:
		resetPlan4生产效率实验单件流()
	case 生产平衡实验:
		resetPlan4Balance()
	case 良品率多件流实验:
		resetPlan4良品率多件流实验()
	case 良品率单件流实验:
		resetPlan4良品率单件流实验()
		// resetPlan4QuanlifiedRate()
	}
}

func updateOrderCompletation(productionInfoList ProductionInfoList) bool {
	// func updateOrderCompletation(offLineProductCountQcPassed int) {
	if 运行实验状态 == 生产效率实验多件流 || 生产效率实验单件流 == 运行实验状态 { //只有在这两种状态时需要订单
		if orderList, ok := g_orderListMap[运行实验状态]; ok {
			if bCompleted := orderList.updateCompletationStatus(productionInfoList); bCompleted {
				updateOrderCoverResults(orderList)
				// BroadcastMessage_OrderCompleted()
				leftProductionInfoList := orderList.checkInventory(productionInfoList)
				beego.Debug(运行实验状态.toString() + " 的所有订单已经完成，剩余产成品如下 " + getFileLocation())
				beego.Emergency(leftProductionInfoList.printSelf())
				return true
			} else {
				updateOrderCoverResults(orderList)
			}
		} else {
			beego.Error("系统错误" + getFileLocation())
		}
	}
	return false
}
func updateOrderCoverResults(orderList OrderList) {
	if orderCoverResults, ok := g_OrderCoverResultsMap[运行实验状态]; ok {
		newResult := orderCoverResults.addResult(运行实验状态, 运行实验状态.toString(), orderList.getCoverRate())
		beego.Debug("更新订单完成率信息" + getFileLocation())
		beego.Emergency(orderCoverResults.printSelf())
		g_OrderCoverResultsMap[运行实验状态] = orderCoverResults
		BroadcastOrderCoverMessage(OrderCoverResults{newResult}) //发送订单完成信息
	} else {
		beego.Error("系统错误" + getFileLocation())
	}
}
func completeSomePlan(count int) {
	if currentPlanList, ok := g_PlanListMap[运行实验状态]; ok {
		for i := 0; i < count; i++ {
			currentPlanList.completeCurrentPlan()
		}
		if currentPlanList.countUncompleted() <= 0 {
			beego.Alert("所有生产计划已经完成" + getFileLocation())
			// beego.Emergency(currentPlanList.printSelf())
			BroadcastMessage_PlanCompleted()
			// setRunningState(未设定) //计划已经完成，关闭流程
		}
	} else {
		beego.Error("completeSomePlan =>  系统运行错误" + getFileLocation())
	}
}
func setAllOnlineProductsUnquanlified() {
	BroadcastMessage_SysTip("产品质量经检验不合格，当前在线半成品均标记为不合格品，请调整产线重新开始生产")
	//将当前已经开始的流程全部标记为结束
	if processLoopList, ok := g_processLoopListMap[运行实验状态]; ok {

		count := processLoopList.setAllOnlineProductsUnquanlified()
		g_processLoopListMap[运行实验状态] = processLoopList
		//将批次与计划统一
		completeSomePlan(count)
	} else {
		beego.Error("setAllOnlineProductsUnquanlified =>  系统运行错误" + getFileLocation())

	}
}
func getFileLocation() string {
	_, file, line, ok := runtime.Caller(1)
	if ok {
		array := strings.Split(file, "/")
		return fmt.Sprintf(" (%s %d)", array[len(array)-1], line)
	} else {
		return "  ???"
	}
}
