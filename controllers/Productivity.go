package controllers

import (
	// "encoding/json"
	"fmt"
	// "github.com/astaxie/beego"
	// "startbeego/models"
	// "strconv"
	"time"
	// "math/rand"
)

type Productivity struct {
	GameStateFlag gameState
	// NpieceValue                    int
	Time                           string
	Timestamp                      int
	OnLineProductCount             int
	OffLineProductCount            int
	OffLineProductCountQcNotPassed int
	ProductTypeIndex               int
	ProductName                    string
	// ProductionInfoList ProductionInfoList
}

func (this Productivity) toString() string {
	return fmt.Sprintf("%+v", this)
}

type Productivities []Productivity

// func (this *Productivities) addProductivity(productionInfoList ProductionInfoList) Productivities {
func (this *Productivities) addProductivity(productionInfoList ProductionInfoList, state gameState) Productivities {
	// func (this *Productivities) addProductivity(productTypeIndex, onLineProductCount, offLineProductCount, offLineProductCountQcNotPassed int) Productivity {
	productivities := Productivities{}
	t := time.Now()
	for i := 0; i < len(productionInfoList); i++ {
		proInfo := productionInfoList[i]
		pro := Productivity{
			// NpieceValue: g_NpieceValue,
			GameStateFlag: state,
			Time:          t.Format("15:04:05"),
			// Time:                           t.Format("2006-01-02 15:04:05"),
			Timestamp:                      int(t.Unix() - originalTimeStamp),
			OnLineProductCount:             proInfo.OnLineProductCount,
			OffLineProductCount:            proInfo.OffLineProductCount,
			OffLineProductCountQcNotPassed: proInfo.OffLineProductCountQcNotPassed,
			ProductTypeIndex:               proInfo.ProductTypeIndex,
			ProductName:                    proInfo.ProductName,
		}
		*this = append(*this, pro)
		productivities = append(productivities, pro)
	}

	// this.printSelf()
	return productivities
}
func (this *Productivities) clearProductivity() {
	*this = Productivities{}
}

// func (this *Productivities) resetProductivity(piece int) {
// 	beego.Trace("resetProductivity remove " + strconv.Itoa(piece))
// 	temp := Productivities{}
// 	for i := 0; i < len(*this); i++ {
// 		if (*this)[i].NpieceValue != piece {
// 			temp = append(temp, (*this)[i])
// 		}
// 	}
// 	*this = temp
// 	this.printSelf()
// }
func (this Productivities) printSelf() string {
	str := ""
	for i := 0; i < len(this); i++ {
		str += this[i].toString() + "\r\n"
		// beego.Trace(this[i].toString())
	}
	return str
}
