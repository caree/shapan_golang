# 精益管理沙盘 #

产品定位在为实时精益管理的流程提供时间测算和统计比较。

产品提供了待优化系统的流程管理功能，针对系统进行细化测量，并提供一个基于Arduino开发的测算工具，用于计量管理中的流程耗时。各个流程的耗时由系统记录下以后，再经过按照精益管理的理念和标准设计的算法处理后，给出当前系统的评分和分析，从而可以根据该结果对系统进行进一步的优化