<!DOCTYPE html>
<meta charset="utf-8" />
<title>开关模拟</title>
    <link rel="icon" 
          type="image/png" 
          href="/images/logo_pure.png">
    <link href="/bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">    
    <script type="text/javascript" src = "/javascripts/jquery.js"></script>
    <script type="text/javascript" src = "/javascripts/underscore.js"></script>

    <script language="javascript" type="text/javascript">
        var output;
        var buttonStates = [];
        var buttonInProcess = []
        function init() {
            output = document.getElementById("output");
            buttonStates.push({name: 'btn001', state: 'off'})
            buttonStates.push({name: 'btn002', state: 'off'})
            buttonStates.push({name: 'btn003', state: 'off'})
            buttonStates.push({name: 'btn256', state: 'off'})

            buttonInProcess = _.without(_.pluck(buttonStates, 'name'), '')
            // buttonInProcess = _.without(_.pluck(buttonStates, 'name'), 'btn256')
        }
        function writeToScreen(message) {
            var pre = document.createElement("p");
            pre.style.wordWrap = "break-word";
            pre.innerHTML = message;
            output.appendChild(pre);
        }
        function pushButton(btnName){
            var stateInfo = _.findWhere(buttonStates, {name: btnName})
            if(stateInfo != null){
                if(stateInfo.state == "off" || stateInfo.state == "yas"){
                    stateInfo.state = "on"
                    highlightButton(stateInfo.name, true)
                }else{
                    stateInfo.state = "off"
                    highlightButton(stateInfo.name, false)
                }
                console.log(btnName + " => " + stateInfo.state)
                $.post('../AcceptSimulatorMessage', stateInfo)
            }
            // var str = btnName + " " + state
            // writeToScreen(str)
        }
        function pushButtonNotQuanlified(btnName){
            var stateInfo = _.findWhere(buttonStates, {name: btnName})
            if(stateInfo != null){
                if(stateInfo.state == "on"){
                    stateInfo.state = "yas"
                    console.log(btnName + " => " + stateInfo.state)
                    highlightButton(btnName, false)
                    $.post('../AcceptSimulatorMessage', stateInfo)
                }
            }            
        }
        function highlightButton(btnName, b){
            if(_.contains(buttonInProcess, btnName)){
                if(b){
                    $('#'+ btnName).removeClass("btn-default")
                    $('#'+ btnName).addClass("btn-success")
                }else{
                    $('#'+ btnName).removeClass("btn-success")
                    $('#'+ btnName).addClass("btn-default")
                }
            }
        }
        window.addEventListener("load", init, false);
    </script>

<!-- <h2>N件流</h2> -->
<body> 
    <div style="text-align: center; font-size: 20px; margin-bottom: 10px;">开关模拟</div>
    <div class="container" >

        <div class="row"  id = "row_container">
            <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12" style="margin-top: 30px;text-align:center;">
                <button type="button" id = "btn001" class="btn btn-default btn-sm" style="width: 150px;" onclick = "pushButton('btn001')">按钮一</button>
            </div>
        </div>

        <div class="row"  id = "row_container">
            <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12" style="margin-top: 30px;text-align:center;">
                <button type="button" id = "btn002" class="btn btn-default btn-sm" style="width: 150px;" onclick = "pushButton('btn002')">按钮二</button>
            </div>
        </div>



        <div class="row"  id = "row_container">
            <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12" style="margin-top: 30px;text-align:center;">
                <button type="button" id = "btn256" class="btn btn-default btn-sm" style="width: 150px;" onclick = "pushButton('btn256')">质检合格按钮</button>
                <button type="button" id = "btn256" class="btn btn-default btn-sm" style="width: 150px;" onclick = "pushButtonNotQuanlified('btn256')">质检不合格按钮</button>
            </div>
        </div>
        <div class="row"  id = "row_container">
            <div class="col-xs-12 col-sm-12 col-md-12 col-md-lg-12" style="margin-top: 30px;text-align:center;">
                <button type="button" id = "btn003" class="btn btn-default btn-sm" style="width: 150px;" onclick = "pushButton('btn003')">按钮三</button>
            </div>
        </div>
    </div>
    <script src="/bootstrap/js/bootstrap.min.js"></script>
</body>
<div id="output"></div>
</html>
  
  