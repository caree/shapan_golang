package controllers

import (
	// "encoding/json"
	"fmt"
	"github.com/astaxie/beego"
	// "startbeego/models"
	"strconv"
	// "time"
	// "math/rand"
)

type ProcessBalanceResult struct {
	// ProductionProcess
	// state      string
	// startStamp int64
	// endStamp   int64
	// startTime  string
	// endTime    string
	LoopIndex  int    `json:"loopIndex"`
	LoopName   string `json:"loopName"`
	Index      int    `json:"index"`
	Name       string `json:"name"`
	TimeElapse int    `json:"timeElapse"`
	// DeviceID string `json:"deviceID"`
	// Note     string `json:"note"`
}

func (this ProcessBalanceResult) toString() string {
	return fmt.Sprintf("%+v", this)
}
func (this *ProcessBalanceResult) updateTimeElapse(_timeElapse int) {
	(*this).TimeElapse = _timeElapse
}

type ProcessBalanceResults []ProcessBalanceResult

func (this *ProcessBalanceResults) findResult(loopIndex, index int) (bool, *ProcessBalanceResult) {
	for i := 0; i < len(*this); i++ {
		result := (*this)[i]
		if result.LoopIndex == loopIndex && result.Index == index {
			// result.updateTimeElapse(timeEnd - timeStart)
			// (&result).TimeElapse = timeEnd - timeStart
			// beego.Warn(result.toString())
			// updated = true
			// break
			return true, &((*this)[i])
		}
	}
	return false, &((*this)[0])
}
func (this *ProcessBalanceResults) addResult(result ProcessBalanceResult) {
	if result.Index == 1 || result.Index == 256 {
		return
	}
	(*this) = append((*this), result)

	tempList := ProcessBalanceResults{}
	for i := 0; i < len(*this); i++ {
		resultTemp := (*this)[i]
		if resultTemp.LoopIndex == (result).LoopIndex && resultTemp.Index != 256 {
			tempList = append(tempList, resultTemp)
		}
	}
	BroadcastBalanceMessage(tempList)
}

func (this *ProcessBalanceResults) printSelf() {
	for i := 0; i < len(*this); i++ {
		result := (*this)[i]
		beego.Trace(result.toString())
	}
}
func (this *ProcessBalanceResults) clearResults() {
	(*this) = ProcessBalanceResults{}
}
func (this *ProcessBalanceResults) addNewLoopResult(loopIndex int, process ProductionProcess) {
	result := ProcessBalanceResult{
		LoopIndex:  loopIndex,
		LoopName:   "生产批次" + strconv.Itoa(loopIndex),
		Index:      process.Index,
		Name:       process.Name,
		TimeElapse: 0}
	(*this) = append((*this), result)
}
